<?php echo widget('Admin.Common')->header(); ?>
    <?php echo widget('Admin.Common')->top(); ?>
    <?php echo widget('Admin.Menu')->leftMenu(); ?>
    <div class="content">
        <?php echo widget('Admin.Menu')->contentMenu(); ?>
        <?php echo widget('Admin.Common')->crumbs(); ?>
        <div class="main-content">

          <ul class="nav nav-tabs">
            <li class="active"><a href="#home" data-toggle="tab">填写应用信息</a></li>
          </ul>

          <div class="row">
            <div class="col-md-4">
              <br>
              <div id="myTabContent" class="tab-content">
                <div class="tab-pane active in" id="home">
                  <form id="tab" target="hiddenwin" method="post" action="<?php echo $formUrl; ?>">
                    <div class="form-group input-group-sm">
                      <label>应用名</label>
                      <input type="text" value="<?php if(isset($info['name'])) echo $info['name']; ?>" name="data[name]" class="form-control">
                    </div>
                    <div class="form-group input-group-sm">
                      <label>appId</label>
                      <input type="text" value="<?php if(isset($info['id'])) echo $info['id']; ?>" name="data[id]" class="form-control" placeholder="请保持唯一" >
                    </div>
                    <div class="form-group">
                      <label>app秘钥</label>
                      <textarea name="data[secret]" rows="3" class="form-control"><?php if(isset($info['secret'])) echo $info['secret']; ?></textarea>
                    </div>
                    <div class="form-group">
                      <label>回调地址</label>
                      <input type="text" value="<?php if(isset($info['redirect_uri'])) echo $info['redirect_uri']; ?>" name="data[redirect_uri]" class="form-control">
                    </div>
                    <div class="btn-toolbar list-toolbar">
                      <a class="btn btn-primary btn-sm sys-btn-submit" data-loading="保存中..." ><i class="fa fa-save"></i> <span class="sys-btn-submit-str">保存</span></a>
                    </div>
                    <?php if(isset($id)): ?>
                      <input name="data[oldid]" type="hidden" value="<?php echo $id;?>" />
                    <?php endif; ?>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <?php echo widget('Admin.Common')->footer(); ?>
        </div>
    </div>
<?php echo widget('Admin.Common')->htmlend(); ?>