<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>系统管理</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="<?php echo csrf_token(); ?>" />

    <link rel="stylesheet" type="text/css" href="<?php echo loadStatic('/lib/bootstrap/css/bootstrap.css'); ?>">
    <link rel="stylesheet" href="<?php echo loadStatic('/lib/font-awesome/css/font-awesome.css'); ?>">

    <script src="<?php echo loadStatic('/lib/jquery-1.11.1.min.js'); ?>" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="<?php echo loadStatic('/stylesheets/theme.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo loadStatic('/stylesheets/premium.css'); ?>">
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
            color: #fff;
        }
        .none {
            display: none;
        }
        .notic {
            color: red;
            display: none;
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<?php echo loadStatic('/lib/html5.js'); ?>"></script>
      <script type="text/javascript">
        $(document).ready(function(){
            $('body').html('您使用的是IE浏览器，仅支持IE9以上版本，建议使用firefox、chrome浏览器，以或得最佳体验。');
        });
        
      </script>
    <![endif]-->
  
</head>
<body class="theme-blue">

    <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
    <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
    <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
    <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!--> 
   
    <!--<![endif]-->

    <div class="dialog">
    <div class="panel panel-default">
        <p class="panel-heading no-collapse">用户注册</p>
        <div id="reg-form" class="panel-body">
            <div class="form-group notic" id="msg"></div>
            <div class="form-group input-group-sm">
                <label>用户名</label>
                <input type="text" class="form-control span12 reg-form" id="username">
            </div>
            <div class="form-group input-group-sm">
            <label>登陆密码</label>
                <input type="password" class="form-control span12 reg-form" id="password">
            </div>

            <div class="form-group input-group-sm">
            <label>密码确认</label>
                <input type="password" class="form-control span12 reg-form" id="password_confirm">
            </div>

            <div class="form-group input-group-sm">
            <label>真实姓名</label>
                <input type="realname" class="form-control span12 reg-form" id="realname">
            </div>
            <input type="hidden" id="redirectUrl" value="<?php echo $redirectUri; ?>" />
            <a href="javascript:;" class="btn btn-sm btn-primary pull-right" id="reg-button">注册</a>
            <div class="clearfix"></div>
        </div>
    </div>
    </div>
    <div style="color:#bbb;font-size:12px; margin:0 auto; width:200px;">最佳体验在IE9+、firefox、chrome</div>
    <script src="<?php echo loadStatic('/lib/bootstrap/js/bootstrap.js'); ?>"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.reg-form').keyup(function(event){
                if (event.keyCode == 13) {
                    $('#submit').trigger('click');
                }
            });

            $('#reg-button').click(function() {
                var username = $('#username').val();
                var password = $('#password').val();
                var password_confirm = $('#password_confirm').val();
                var realname = $('#realname').val();
                if(username == '' || password == '' || password_confirm == '' || realname =='') {
                    $('#msg').html('请完善所有信息').show();
                    return false;
                }
                if(password != password_confirm) {
                    $('#msg').html('密码不一致').show();
                    return false;
                }
                $.ajax({
                    type: "get",
                    url: '<?php echo route('oauth.reg.post'); ?>',
                    data: {username:username, password:password, password_confirm:password_confirm, realname:realname},
                    dataType: "jsonp",
                    jsonp: "callback",
                    jsonpCallback:"callback",
                    headers: {
                        'X-XSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        if(data.result == false) {
                            $('#msg').html(data.msg).show();
                        } else {
                            var redirectUrl = $('#redirectUrl').val();
                            $('body').html('注册成功！<a href="'+redirectUrl+'">返回</a>');
                        }
                    },
                    beforeSend: function() {
                        $('#msg').html('处理中...').show();
                    },
                    timeout: 10000,
                    complete: function(request, status) {
                        if(status == 'timeout') {
                            $('#msg').html('超时了，请重试').show();
                        }
                    }
                });
            });
        });

    </script>
  
</body></html>