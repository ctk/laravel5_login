<?php

return array(
    'please_input_username' => '请输入用户名',
    'please_input_password' => '请输入密码',
    'please_input_realname' => '请输入真实姓名',
    'password_not_same' => '密码不一致',
    'account_exists' => '用户已经注册',
    'reg_success' => '注册成功',
);