<?php

use App\Services\Routes as RoutesManager;

// 后台相关
$routesManager = new RoutesManager();
$routesManager->admin();

// 单点登陆相关，主要用于应用需要登陆的接入。

/**
 * 登陆，如果已经登陆那么显示是否授权给第三方
 */
Route::get('/oauth/authorize', [ 'as' => 'oauth.authorize.get','middleware' => ['check-authorization-params'], 'uses' => 'Oauth\AuthorizeController@authorize' ]);

/**
 * 处理登陆的
 */
Route::get('/ologin/proc', [ 'as' => 'oauth.login.proc', 'uses' => 'Oauth\AuthorizeController@proc' ]);
Route::get('/ologin/pre', [ 'as' => 'oauth.login.pre', 'uses' => 'Oauth\AuthorizeController@prelogin' ]);

/**
 * 处理上一步的授权逻辑，同意的话会返回一个code给客户端，不同意的话会返回客户端错误提示
 */
Route::post('/oauth/authorize', ['as' => 'oauth.authorize.post','middleware' => ['check-authorization-params', 'csrf'], 'uses' => 'Oauth\AuthorizeController@postAuthorize']);

/**
 * 通过第一步得到的code，申请access_token
 */
Route::post('/oauth/access_token', ['as' => 'oauth.access_token.get', function() {
    return Response::json(Authorizer::issueAccessToken());
}]);

/**
 * 具体的通过access_token拿资料接口了，这里只是一个例子
 */
Route::get('/oauth/detail', ['as' => 'oauth.detail', 'middleware' => ['oauth'], 'uses' => 'Oauth\AuthorizeController@userDetail']);

/**
 * 注册的
 */
Route::get('/oauth/reg', [ 'as' => 'oauth.reg', 'uses' => 'Oauth\AuthorizeController@reg' ]);
Route::get('/oauth/regdo', [ 'as' => 'oauth.reg.post', 'uses' => 'Oauth\AuthorizeController@regDo' ]);