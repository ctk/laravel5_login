<?php namespace App\Http\Controllers\Admin\App;

use App\Http\Controllers\Admin\Controller;
use App\Models\Admin\App as AppModel;
use App\Models\Admin\AppEp as AppEpModel;
use App\Services\Admin\App\Param\Save as AppSave;
use App\Services\Admin\App\Process as AppProcess;
use App\Libraries\Js;
use Session, Request, Lang;

/**
 * oauth应用管理相关
 *
 * @author jiang <mylampblog@163.com>
 */
class ManagerController extends Controller
{
    /**
     * oauth应用模型
     * 
     * @var object
     */
    private $appModel;

    /**
     * app save
     * 
     * @var object
     */
    private $appSave;

    /**
     * app process
     * 
     * @var object
     */
    private $appProcess;

    /**
     * app model
     * 
     * @var object
     */
    private $appEpModel;

    /**
     * 初始化
     */
    public function __construct()
    {
        $this->appModel = new AppModel();
        $this->appSave = new AppSave();
        $this->appProcess = new AppProcess();
        $this->appEpModel = new AppEpModel();
    }

    /**
     * 应用列表
     */
    public function index()
    {
        Session::flashInput(['http_referer' => Request::fullUrl()]);
        $list = $this->appModel->getAllByPage();
        $page = $list->setPath('')->appends(Request::all())->render();
        return view('admin.app.index', compact('list', 'page'));
    }

    /**
     * 增加新的应用
     */
    public function add()
    {
        if(Request::method() == 'POST') return $this->saveDatasToDatabase();
        $formUrl = R('common', 'app.manager.add');
        return view('admin.app.add', compact('formUrl'));
    }

    /**
     * 增加新的应用
     *
     * @access private
     */
    private function saveDatasToDatabase()
    {
        $data = (array) Request::input('data');
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');

        $this->appSave->setAttributes($data);
        if($this->appProcess->addApp($this->appSave) !== false) {
            return Js::locate(R('common', 'app.manager.index'), 'parent');
        }
        return Js::error($this->appProcess->getErrorMessage());
    }

    /**
     * 编辑应用
     *
     * @access public
     */
    public function edit()
    {
        if(Request::method() == 'POST') {
            return $this->updateDatasToDatabase();
        }

        $id = Request::input('id');

        $info = $this->appModel->getOneById($id);
        $epInfo = $this->appEpModel->getOneByClientId($id);
        $info['redirect_uri'] = $epInfo['redirect_uri'];

        if(empty($info)) {
            return Js::error(Lang::get('common.illegal_operation'));
        }

        $formUrl = R('common', 'app.manager.edit');

        return view('admin.app.add',
            compact('info', 'formUrl', 'id')
        );
    }
    
    /**
     * 编辑应用
     *
     * @access private
     */
    private function updateDatasToDatabase()
    {
        $data = Request::input('data');
        $oldid = $data['oldid'];

        if( ! $data or ! is_array($data)) {
            return Js::error(Lang::get('common.illegal_operation'));
        }

        $this->appSave->setAttributes($data);

        if($this->appProcess->editApp($this->appSave, $oldid)) {
            return Js::locate(R('common', 'app.manager.index'), 'parent');
        }

        return Js::error($this->appProcess->getErrorMessage());
    }

    /**
     * 删除应用
     *
     * @access public
     */
    public function delete()
    {
        $id = (array) Request::input('id');
        if( ! $id ) return responseJson(Lang::get('common.action_error'));
        if($this->appProcess->detele(['ids' => $id])) {
            return responseJson(Lang::get('common.action_success'), true);
        }
        
        return responseJson($this->appProcess->getErrorMessage());
    }

}