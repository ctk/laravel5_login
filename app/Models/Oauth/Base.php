<?php namespace App\Models\Oauth;

use Illuminate\Database\Eloquent\Model;

/**
 * 模型基类
 */
class Base extends Model
{
    /**
     * 关闭自动维护updated_at、created_at字段
     * 
     * @var boolean
     */
    public $timestamps = false;
}