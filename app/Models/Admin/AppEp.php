<?php namespace App\Models\Admin;

use App\Models\Admin\Base;

/**
 * oauth应用模型
 *
 * @author jiang
 */
class AppEp extends Base
{
    /**
     * 表名
     *
     * @var string
     */
    protected $table = 'oauth_client_endpoints';

    /**
     * 可以被集体附值的表的字段
     *
     * @var string
     */
    protected $fillable = array('client_id', 'redirect_uri', 'id', 'created_at', 'updated_at');
    
    /**
     * 增加应用
     * 
     * @param array $data 所需要插入的信息
     */
    public function addAppEp(array $data)
    {
        return $this->create($data);
    }
    
    /**
     * 修改应用
     * 
     * @param array $data 所需要插入的信息
     */
    public function editAppEp(array $data, $clientId)
    {
        return $this->where('client_id', '=', $clientId)->update($data);
    }

    /**
     * 取得指定ID信息
     * 
     * @param string $clientId ID
     * @return array
     */
    public function getOneByClientId($clientId)
    {
        return $this->where('client_id', '=', $clientId)->first();
    }

}
