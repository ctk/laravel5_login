<?php namespace App\Models\Admin;

use App\Models\Admin\Base;

/**
 * oauth应用模型
 *
 * @author jiang
 */
class App extends Base
{
    /**
     * 表名
     *
     * @var string
     */
    protected $table = 'oauth_clients';

    /**
     * 可以被集体附值的表的字段
     *
     * @var string
     */
    protected $fillable = array('id', 'secret', 'name', 'created_at', 'updated_at');
    
    /**
     * 取得所有的应用信息
     *
     * @return array
     */
    public function getAllByPage()
    {
        $currentQuery = $this->select(['oauth_clients.*', 'oauth_client_endpoints.redirect_uri'])->leftJoin('oauth_client_endpoints', 'oauth_client_endpoints.client_id', '=', 'oauth_clients.id')->paginate(self::PAGE_NUMS);
        return $currentQuery;
    }

    /**
     * 取得指定ID信息
     * 
     * @param intval $id ID
     * @return array
     */
    public function getOneById($id)
    {
        return $this->where('id', '=', $id)->first();
    }

    /**
     * 增加应用
     * 
     * @param array $data 所需要插入的信息
     */
    public function addApp(array $data)
    {
        return $this->create($data);
    }
    
    /**
     * 修改应用
     * 
     * @param array $data 所需要插入的信息
     */
    public function editApp(array $data, $id)
    {
        return $this->where('id', '=', $id)->update($data);
    }
    
    /**
     * 删除应用
     * 
     * @param array $id ID
     */
    public function deleteApp(array $ids)
    {
        return $this->destroy($ids);
    }

}
