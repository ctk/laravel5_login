<?php namespace App\Services\Oauth;

use Session;

class SC {

    /**
     * 用户登录的session key
     */
    CONST OAUTH_LOGIN_MARK_SESSION_KEY = 'OAUTH_LOGIN_MARK_SESSION';

    /**
     * 密钥 session key
     */
    CONST OAUTH_PUBLIC_KEY = 'OAUTH_LOGIN_PROCESS_PUBLIC';

    /**
     * 验证的时候附带的参数
     */
    CONST OAUTH_PARAMS = 'OAUTH_PARAMS';

    /**
     * 设置登录成功的session
     * 
     * @param array $userInfo 用户的相关信息
     */
    static public function setLoginSession($userInfo)
    {
        return Session::put(self::OAUTH_LOGIN_MARK_SESSION_KEY, $userInfo);
    }

    /**
     * 返回登录成功的session
     */
    static public function getLoginSession()
    {
        return Session::get(self::OAUTH_LOGIN_MARK_SESSION_KEY);
    }

    /**
     * 删除登录的session
     * 
     * @return void
     */
    static public function delLoginSession()
    {
        Session::forget(self::OAUTH_LOGIN_MARK_SESSION_KEY);
        //Session::flush();
        //Session::regenerate();
    }

    /**
     * 设置并返回加密密钥
     *
     * @return string 密钥
     */
    static public function setPublicKey()
    {
        $key = uniqid();
        Session::put(self::OAUTH_PUBLIC_KEY, $key);
        return $key;
    }

    /**
     * 取得刚才设置的加密密钥
     * 
     * @return string 密钥
     */
    static public function getPublicKey()
    {
        return Session::get(self::OAUTH_PUBLIC_KEY);
    }

    /**
     * 删除密钥
     * 
     * @return void
     */
    static public function delPublicKey()
    {
        Session::forget(self::OAUTH_PUBLIC_KEY);
        //Session::flush();
        //Session::regenerate();
    }

    /**
     * for cache
     */
    static public function setOauthParams($params)
    {
        return Session::put(self::OAUTH_PARAMS, $params);
    }

    /**
     * for cache
     */
    static public function getOauthParams()
    {
        return Session::get(self::OAUTH_PARAMS);
    }

}