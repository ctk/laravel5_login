<?php namespace App\Services\Admin\App\Validate;

use Validator, Lang;
use App\Services\BaseValidate;

/**
 * oauth app
 *
 * @author jiang <mylampblog@163.com>
 */
class App extends BaseValidate
{
    /**
     * validate
     *
     * @access public
     */
    public function add(\App\Services\Admin\App\Param\Save $data)
    {
        // 创建验证规则
        $rules = array(
            'id' => 'required',
            'secret' => 'required',
            'name' => 'required',
            'redirect_uri' => 'required',
        );
        
        // 自定义验证消息
        $messages = array(
            'id.required' => Lang::get('app.complete_all_info'),
            'secret.required' => Lang::get('app.complete_all_info'),
            'name.required' => Lang::get('app.complete_all_info'),
            'redirect_uri.required' => Lang::get('app.complete_all_info'),
        );
        
        //开始验证
        $validator = Validator::make($data->toArray(), $rules, $messages);
        if($validator->fails())
        {
            $this->errorMsg = $validator->messages()->first();
            return false;
        }
        return true;
    }
    
    /**
     * validate
     *
     * @access public
     */
    public function edit(\App\Services\Admin\App\Param\Save $data)
    {
        return $this->add($data);
    }
    
}
