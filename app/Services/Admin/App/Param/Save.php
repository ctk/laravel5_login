<?php namespace App\Services\Admin\App\Param;

use App\Services\AbstractParam;

/**
 * oauth app
 *
 * @author jiang <mylampblog@163.com>
 */
class Save extends AbstractParam
{
    protected $id;

    protected $secret;

    protected $name;

    protected $created_at;

    protected $updated_at;

    protected $redirect_uri;

}
