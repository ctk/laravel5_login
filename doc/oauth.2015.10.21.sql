/*
Navicat MySQL Data Transfer

Source Server         : 192.168.93.209
Source Server Version : 50621
Source Host           : 192.168.93.209:3306
Source Database       : oauth

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-10-21 09:51:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sso_access`
-- ----------------------------
DROP TABLE IF EXISTS `sso_access`;
CREATE TABLE `sso_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL COMMENT '角色的ID',
  `permission_id` int(11) NOT NULL COMMENT '节点的ID',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '标识是用户组还是用户1为用户组2为用户,默认为用户组',
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `node_id` (`permission_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=405 DEFAULT CHARSET=utf8 COMMENT='权限表_by_jiang';

-- ----------------------------
-- Records of sso_access
-- ----------------------------
INSERT INTO `sso_access` VALUES ('379', '2', '1', '1');
INSERT INTO `sso_access` VALUES ('380', '2', '67', '1');
INSERT INTO `sso_access` VALUES ('381', '2', '4', '1');
INSERT INTO `sso_access` VALUES ('382', '2', '29', '1');
INSERT INTO `sso_access` VALUES ('394', '3', '1', '2');
INSERT INTO `sso_access` VALUES ('395', '3', '55', '2');
INSERT INTO `sso_access` VALUES ('396', '3', '73', '2');
INSERT INTO `sso_access` VALUES ('397', '3', '56', '2');
INSERT INTO `sso_access` VALUES ('398', '3', '57', '2');
INSERT INTO `sso_access` VALUES ('399', '3', '58', '2');
INSERT INTO `sso_access` VALUES ('400', '3', '59', '2');
INSERT INTO `sso_access` VALUES ('401', '3', '60', '2');
INSERT INTO `sso_access` VALUES ('402', '3', '61', '2');
INSERT INTO `sso_access` VALUES ('403', '3', '62', '2');
INSERT INTO `sso_access` VALUES ('404', '3', '63', '2');

-- ----------------------------
-- Table structure for `sso_action_log`
-- ----------------------------
DROP TABLE IF EXISTS `sso_action_log`;
CREATE TABLE `sso_action_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '登录的用户名',
  `user_id` int(11) NOT NULL COMMENT '登录的用户ID',
  `ip` varchar(20) NOT NULL COMMENT 'ip地址',
  `ip_adress` varchar(255) NOT NULL COMMENT 'ip所属的地区',
  `add_time` int(11) NOT NULL,
  `realname` varchar(30) NOT NULL COMMENT '真实姓名',
  `content` text NOT NULL COMMENT '记录的内容',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `username` (`username`) USING BTREE,
  KEY `addtime` (`add_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=364 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sso_action_log
-- ----------------------------
INSERT INTO `sso_action_log` VALUES ('1', 'admin', '1', '127.0.0.1', '', '1433318628', '管理员', '');
INSERT INTO `sso_action_log` VALUES ('2', 'admin', '1', '127.0.0.1', '', '1433319059', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('3', 'admin', '1', '127.0.0.1', '', '1433319146', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('4', 'test', '2', '127.0.0.1', '', '1433323585', 'test', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('5', 'admin', '1', '127.0.0.1', '', '1433323596', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('6', 'test', '2', '127.0.0.1', '', '1433387021', 'test', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('7', 'admin', '1', '127.0.0.1', '', '1433387092', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('8', 'admin', '1', '127.0.0.1', '', '1433387394', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('9', 'admin', '1', '127.0.0.1', '', '1433387489', '管理员', '增加了新的系统用户：test1231231123');
INSERT INTO `sso_action_log` VALUES ('10', 'admin', '1', '127.0.0.1', '', '1433388058', '管理员', '编辑了系统用户：test1231231123');
INSERT INTO `sso_action_log` VALUES ('11', 'admin', '1', '127.0.0.1', '', '1433389439', '管理员', '增加了新的系统用户：123123aab');
INSERT INTO `sso_action_log` VALUES ('12', 'admin', '1', '127.0.0.1', '', '1433389488', '管理员', '增加了新的系统用户：12313123123');
INSERT INTO `sso_action_log` VALUES ('13', 'admin', '1', '127.0.0.1', '', '1433389702', '管理员', '增加了新的系统用户：123123ffffffff');
INSERT INTO `sso_action_log` VALUES ('14', 'admin', '1', '127.0.0.1', '', '1433389728', '管理员', '增加了新的系统用户：123123gg');
INSERT INTO `sso_action_log` VALUES ('15', 'admin', '1', '127.0.0.1', '', '1433390103', '管理员', '删除了系统用户：12313123123');
INSERT INTO `sso_action_log` VALUES ('16', 'admin', '1', '127.0.0.1', '', '1433390300', '管理员', '删除了系统用户：123123aab');
INSERT INTO `sso_action_log` VALUES ('17', 'admin', '1', '127.0.0.1', '', '1433390407', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('18', 'test', '2', '127.0.0.1', '', '1433390430', 'test', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('19', 'admin', '1', '127.0.0.1', '', '1433397520', '管理员', '增加了新的用户组：sdfasf');
INSERT INTO `sso_action_log` VALUES ('20', 'admin', '1', '127.0.0.1', '', '1433398047', '管理员', '编辑了用户组：sdfasf');
INSERT INTO `sso_action_log` VALUES ('21', 'admin', '1', '127.0.0.1', '', '1433398364', '管理员', '删除了系统用户：sdfasf');
INSERT INTO `sso_action_log` VALUES ('22', 'admin', '1', '127.0.0.1', '', '1433398384', '管理员', '删除了用户组：test123');
INSERT INTO `sso_action_log` VALUES ('23', 'admin', '1', '127.0.0.1', '', '1433398933', '管理员', '增加了新的用户组：:groupname');
INSERT INTO `sso_action_log` VALUES ('24', 'admin', '1', '127.0.0.1', '', '1433398963', '管理员', '改变了用户组的权限：测试用户组');
INSERT INTO `sso_action_log` VALUES ('25', 'admin', '1', '127.0.0.1', '', '1433399098', '管理员', '改变了用户的权限：test');
INSERT INTO `sso_action_log` VALUES ('26', 'admin', '1', '127.0.0.1', '', '1433400975', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('27', 'admin', '1', '127.0.0.1', '', '1433408366', '管理员', '增加了新的系统用户：test123');
INSERT INTO `sso_action_log` VALUES ('28', 'admin', '1', '127.0.0.1', '', '1433408371', '管理员', '删除了系统用户：test123');
INSERT INTO `sso_action_log` VALUES ('29', 'admin', '1', '127.0.0.1', '', '1433408376', '管理员', '改变了用户的权限：test');
INSERT INTO `sso_action_log` VALUES ('30', 'admin', '1', '127.0.0.1', '', '1433904485', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('31', 'admin', '1', '127.0.0.1', '', '1433904731', '管理员', '增加了新的用户组：1123');
INSERT INTO `sso_action_log` VALUES ('32', 'admin', '1', '127.0.0.1', '', '1433904744', '管理员', '删除了用户组：1123');
INSERT INTO `sso_action_log` VALUES ('33', 'test', '2', '127.0.0.1', '', '1433905009', 'test', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('34', 'admin', '1', '127.0.0.1', '', '1433907007', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('35', 'admin', '1', '127.0.0.1', '', '1433909004', '管理员', '增加了新的用户组：1123');
INSERT INTO `sso_action_log` VALUES ('36', 'admin', '1', '127.0.0.1', '', '1433985688', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('37', 'admin', '1', '127.0.0.1', '', '1434093122', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('38', 'admin', '1', '127.0.0.1', '', '1434094731', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('39', 'admin', '1', '127.0.0.1', '', '1434333022', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('40', 'admin', '1', '127.0.0.1', '', '1434333284', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('41', 'admin', '1', '127.0.0.1', '', '1434334022', '管理员', '增加了新的用户组：123123ssss');
INSERT INTO `sso_action_log` VALUES ('42', 'admin', '1', '127.0.0.1', '', '1434334039', '管理员', '编辑了用户组：123123ssssa');
INSERT INTO `sso_action_log` VALUES ('43', 'admin', '1', '127.0.0.1', '', '1434334965', '管理员', '删除了用户组：123123ssssa');
INSERT INTO `sso_action_log` VALUES ('44', 'admin', '1', '127.0.0.1', '', '1434334970', '管理员', '删除了用户组：1123');
INSERT INTO `sso_action_log` VALUES ('45', 'admin', '1', '127.0.0.1', '', '1434335101', '管理员', '增加了新的工作流：工作流2');
INSERT INTO `sso_action_log` VALUES ('46', 'admin', '1', '127.0.0.1', '', '1434335308', '管理员', '增加了新的工作流：工作流3');
INSERT INTO `sso_action_log` VALUES ('47', 'admin', '1', '127.0.0.1', '', '1434336740', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('48', 'admin', '1', '127.0.0.1', '', '1434337228', '管理员', '编辑了工作流：工作流11');
INSERT INTO `sso_action_log` VALUES ('49', 'admin', '1', '127.0.0.1', '', '1434337255', '管理员', '增加了新的工作流：123123');
INSERT INTO `sso_action_log` VALUES ('50', 'admin', '1', '127.0.0.1', '', '1434338302', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('51', 'admin', '1', '127.0.0.1', '', '1434338514', '管理员', '删除了工作流：工作流2');
INSERT INTO `sso_action_log` VALUES ('52', 'admin', '1', '127.0.0.1', '', '1434339359', '管理员', '增加了新的用户组：123');
INSERT INTO `sso_action_log` VALUES ('53', 'admin', '1', '127.0.0.1', '', '1434339370', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('54', 'admin', '1', '127.0.0.1', '', '1434339391', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('55', 'admin', '1', '127.0.0.1', '', '1434339412', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('56', 'admin', '1', '127.0.0.1', '', '1434339442', '管理员', '删除了用户组：123');
INSERT INTO `sso_action_log` VALUES ('57', 'admin', '1', '127.0.0.1', '', '1434339604', '管理员', '增加了新的系统用户：123123ffffffff');
INSERT INTO `sso_action_log` VALUES ('58', 'admin', '1', '127.0.0.1', '', '1434339625', '管理员', '改变了用户的权限：123123ffffffff');
INSERT INTO `sso_action_log` VALUES ('59', 'admin', '1', '127.0.0.1', '', '1434339648', '管理员', '删除了系统用户：123123ffffffff');
INSERT INTO `sso_action_log` VALUES ('60', 'admin', '1', '127.0.0.1', '', '1434339672', '管理员', '改变了用户组的权限：测试用户组');
INSERT INTO `sso_action_log` VALUES ('61', 'admin', '1', '127.0.0.1', '', '1434339687', '管理员', '增加了新的系统用户：123123gg');
INSERT INTO `sso_action_log` VALUES ('62', 'admin', '1', '127.0.0.1', '', '1434339703', '管理员', '改变了用户的权限：123123gg');
INSERT INTO `sso_action_log` VALUES ('63', 'admin', '1', '127.0.0.1', '', '1434339721', '管理员', '删除了系统用户：123123gg');
INSERT INTO `sso_action_log` VALUES ('64', 'admin', '1', '127.0.0.1', '', '1434339734', '管理员', '改变了用户的权限：test');
INSERT INTO `sso_action_log` VALUES ('65', 'admin', '1', '127.0.0.1', '', '1434339747', '管理员', '删除了用户组：测试用户组');
INSERT INTO `sso_action_log` VALUES ('66', 'admin', '1', '127.0.0.1', '', '1434341772', '管理员', '编辑了工作流：工作流1');
INSERT INTO `sso_action_log` VALUES ('67', 'admin', '1', '127.0.0.1', '', '1434345134', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('68', 'admin', '1', '127.0.0.1', '', '1434347265', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('69', 'admin', '1', '127.0.0.1', '', '1434349496', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('70', 'admin', '1', '127.0.0.1', '', '1434352541', '管理员', '增加了新的工作流步骤：财务总监审核');
INSERT INTO `sso_action_log` VALUES ('71', 'admin', '1', '127.0.0.1', '', '1434352641', '管理员', '增加了新的工作流步骤：公司复责人审核');
INSERT INTO `sso_action_log` VALUES ('72', 'admin', '1', '127.0.0.1', '', '1434353123', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('73', 'admin', '1', '127.0.0.1', '', '1434354662', '管理员', '编辑了工作流步骤：公司负责人审核');
INSERT INTO `sso_action_log` VALUES ('74', 'admin', '1', '127.0.0.1', '', '1434354730', '管理员', '编辑了工作流步骤：公司负责人审核');
INSERT INTO `sso_action_log` VALUES ('75', 'admin', '1', '127.0.0.1', '', '1434354742', '管理员', '编辑了工作流步骤：公司负责人审核');
INSERT INTO `sso_action_log` VALUES ('76', 'admin', '1', '127.0.0.1', '', '1434354752', '管理员', '编辑了工作流步骤：公司负责人审核');
INSERT INTO `sso_action_log` VALUES ('77', 'admin', '1', '127.0.0.1', '', '1434354837', '管理员', '增加了新的工作流步骤：xx');
INSERT INTO `sso_action_log` VALUES ('78', 'admin', '1', '127.0.0.1', '', '1434354941', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('79', 'admin', '1', '127.0.0.1', '', '1434355261', '管理员', '删除了工作流步骤：xx');
INSERT INTO `sso_action_log` VALUES ('80', 'admin', '1', '127.0.0.1', '', '1434422513', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('81', 'admin', '1', '127.0.0.1', '', '1434441684', '管理员', '关联了用户test到工作流工作流1中的审核步骤财务审核');
INSERT INTO `sso_action_log` VALUES ('82', 'admin', '1', '127.0.0.1', '', '1434441764', '管理员', '关联了用户：test到工作流：工作流1中的审核步骤：财务审核');
INSERT INTO `sso_action_log` VALUES ('83', 'admin', '1', '127.0.0.1', '', '1434441764', '管理员', '关联了用户：管理员到工作流：工作流1中的审核步骤：财务审核');
INSERT INTO `sso_action_log` VALUES ('84', 'admin', '1', '127.0.0.1', '', '1434441789', '管理员', '关联了用户：test 到工作流：工作流1 中的审核步骤：财务审核');
INSERT INTO `sso_action_log` VALUES ('85', 'admin', '1', '127.0.0.1', '', '1434441789', '管理员', '关联了用户：管理员 到工作流：工作流1 中的审核步骤：财务审核');
INSERT INTO `sso_action_log` VALUES ('86', 'admin', '1', '127.0.0.1', '', '1434441843', '管理员', '关联了用户：test 到工作流：工作流1 中的审核步骤：财务审核');
INSERT INTO `sso_action_log` VALUES ('87', 'admin', '1', '127.0.0.1', '', '1434443074', '管理员', '增加了新的工作流：测试仪');
INSERT INTO `sso_action_log` VALUES ('88', 'admin', '1', '127.0.0.1', '', '1434443280', '管理员', '编辑了工作流：测试仪');
INSERT INTO `sso_action_log` VALUES ('89', 'admin', '1', '127.0.0.1', '', '1434443296', '管理员', '增加了新的工作流步骤：123');
INSERT INTO `sso_action_log` VALUES ('90', 'admin', '1', '127.0.0.1', '', '1434443311', '管理员', '增加了新的工作流步骤：ads');
INSERT INTO `sso_action_log` VALUES ('91', 'admin', '1', '127.0.0.1', '', '1434443332', '管理员', '关联了用户：test 到工作流：测试仪 中的审核步骤：123');
INSERT INTO `sso_action_log` VALUES ('92', 'admin', '1', '127.0.0.1', '', '1434443347', '管理员', '关联了用户：管理员 到工作流：测试仪 中的审核步骤：ads');
INSERT INTO `sso_action_log` VALUES ('93', 'admin', '1', '127.0.0.1', '', '1434443452', '管理员', '增加了新的工作流步骤：1231');
INSERT INTO `sso_action_log` VALUES ('94', 'admin', '1', '127.0.0.1', '', '1434443543', '管理员', '增加了新的工作流步骤：123123123');
INSERT INTO `sso_action_log` VALUES ('95', 'admin', '1', '127.0.0.1', '', '1434443554', '管理员', '删除了工作流步骤：123123123');
INSERT INTO `sso_action_log` VALUES ('96', 'admin', '1', '127.0.0.1', '', '1434443580', '管理员', '删除了工作流：测试仪');
INSERT INTO `sso_action_log` VALUES ('97', 'admin', '1', '127.0.0.1', '', '1434443792', '管理员', '改变了用户的权限：test');
INSERT INTO `sso_action_log` VALUES ('98', 'admin', '1', '127.0.0.1', '', '1434443823', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('99', 'test', '2', '127.0.0.1', '', '1434443878', 'test', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('100', 'test', '2', '127.0.0.1', '', '1434444960', 'test', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('101', 'test', '2', '127.0.0.1', '', '1434444985', 'test', '增加了新的工作流：123');
INSERT INTO `sso_action_log` VALUES ('102', 'test', '2', '127.0.0.1', '', '1434444994', 'test', '编辑了工作流：123');
INSERT INTO `sso_action_log` VALUES ('103', 'test', '2', '127.0.0.1', '', '1434445001', 'test', '删除了工作流：123');
INSERT INTO `sso_action_log` VALUES ('104', 'admin', '1', '127.0.0.1', '', '1434445027', '管理员', '改变了用户的权限：test');
INSERT INTO `sso_action_log` VALUES ('105', 'admin', '1', '127.0.0.1', '', '1434445038', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('106', 'admin', '1', '127.0.0.1', '', '1434507752', '管理员', '关联了用户：test 到工作流：工作流1 中的审核步骤：财务总监审核');
INSERT INTO `sso_action_log` VALUES ('107', 'admin', '1', '127.0.0.1', '', '1434507752', '管理员', '关联了用户：管理员 到工作流：工作流1 中的审核步骤：财务总监审核');
INSERT INTO `sso_action_log` VALUES ('108', 'admin', '1', '127.0.0.1', '', '1434507770', '管理员', '关联了用户：管理员 到工作流：工作流1 中的审核步骤：公司负责人审核');
INSERT INTO `sso_action_log` VALUES ('109', 'admin', '1', '127.0.0.1', '', '1434508916', '管理员', '增加了新的工作流：测试工作流');
INSERT INTO `sso_action_log` VALUES ('110', 'admin', '1', '127.0.0.1', '', '1434508942', '管理员', '编辑了工作流：测试工作流');
INSERT INTO `sso_action_log` VALUES ('111', 'admin', '1', '127.0.0.1', '', '1434509294', '管理员', '编辑了工作流：工作流1');
INSERT INTO `sso_action_log` VALUES ('112', 'admin', '1', '127.0.0.1', '', '1434509313', '管理员', '编辑了工作流：测试工作流');
INSERT INTO `sso_action_log` VALUES ('113', 'admin', '1', '127.0.0.1', '', '1434509520', '管理员', '编辑了工作流：测试工作流');
INSERT INTO `sso_action_log` VALUES ('114', 'admin', '1', '127.0.0.1', '', '1434516742', '管理员', '增加了新的工作流步骤：测试1');
INSERT INTO `sso_action_log` VALUES ('115', 'admin', '1', '127.0.0.1', '', '1434516769', '管理员', '增加了新的工作流步骤：测试2');
INSERT INTO `sso_action_log` VALUES ('116', 'admin', '1', '127.0.0.1', '', '1434516774', '管理员', '关联了用户：管理员 到工作流：测试工作流 中的审核步骤：测试1');
INSERT INTO `sso_action_log` VALUES ('117', 'admin', '1', '127.0.0.1', '', '1434516845', '管理员', '关联了用户：test 到工作流：测试工作流 中的审核步骤：测试1');
INSERT INTO `sso_action_log` VALUES ('118', 'admin', '1', '127.0.0.1', '', '1434516853', '管理员', '关联了用户：test 到工作流：测试工作流 中的审核步骤：测试1');
INSERT INTO `sso_action_log` VALUES ('119', 'admin', '1', '127.0.0.1', '', '1434516853', '管理员', '关联了用户：管理员 到工作流：测试工作流 中的审核步骤：测试1');
INSERT INTO `sso_action_log` VALUES ('120', 'admin', '1', '127.0.0.1', '', '1434517331', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('121', 'admin', '1', '127.0.0.1', '', '1434520918', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('122', 'admin', '1', '127.0.0.1', '', '1434523410', '管理员', '关联了用户：test 到工作流：测试工作流 中的审核步骤：测试2');
INSERT INTO `sso_action_log` VALUES ('123', 'admin', '1', '127.0.0.1', '', '1434523426', '管理员', '编辑了工作流步骤：测试1');
INSERT INTO `sso_action_log` VALUES ('124', 'admin', '1', '127.0.0.1', '', '1434523612', '管理员', '关联了用户：test 到工作流：测试工作流 中的审核步骤：测试1');
INSERT INTO `sso_action_log` VALUES ('125', 'admin', '1', '127.0.0.1', '', '1434523635', '管理员', '关联了用户：管理员 到工作流：测试工作流 中的审核步骤：测试2');
INSERT INTO `sso_action_log` VALUES ('126', 'test', '2', '127.0.0.1', '', '1434523662', 'test', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('127', 'test', '2', '127.0.0.1', '', '1434524881', 'test', '增加了新的工作流：xx');
INSERT INTO `sso_action_log` VALUES ('128', 'test', '2', '127.0.0.1', '', '1434524889', 'test', '删除了工作流：xx');
INSERT INTO `sso_action_log` VALUES ('129', 'test', '2', '127.0.0.1', '', '1434524895', 'test', '删除了工作流：工作流11');
INSERT INTO `sso_action_log` VALUES ('130', 'admin', '1', '127.0.0.1', '', '1435044773', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('131', 'admin', '1', '127.0.0.1', '', '1435114399', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('132', 'admin', '1', '127.0.0.1', '', '1435117746', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('133', 'admin', '1', '127.0.0.1', '', '1435117778', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('134', 'admin', '1', '127.0.0.1', '', '1435118579', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('135', 'admin', '1', '127.0.0.1', '', '1435118762', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('136', 'admin', '1', '127.0.0.1', '', '1435126625', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('137', 'admin', '1', '127.0.0.1', '', '1435127311', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('138', 'admin', '1', '127.0.0.1', '', '1435127360', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('139', 'admin', '1', '127.0.0.1', '', '1435129351', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('140', 'admin', '1', '127.0.0.1', '', '1435288666', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('141', 'admin', '1', '127.0.0.1', '', '1435304943', '管理员', '编辑了用户组：超级用户组');
INSERT INTO `sso_action_log` VALUES ('142', 'admin', '1', '127.0.0.1', '', '1435304988', '管理员', '增加了新的用户组：123');
INSERT INTO `sso_action_log` VALUES ('143', 'admin', '1', '127.0.0.1', '', '1435305005', '管理员', '增加了新的用户组：123');
INSERT INTO `sso_action_log` VALUES ('144', 'admin', '1', '127.0.0.1', '', '1435305072', '管理员', '编辑了工作流：测试工作流');
INSERT INTO `sso_action_log` VALUES ('145', 'admin', '1', '127.0.0.1', '', '1435305087', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('146', 'admin', '1', '127.0.0.1', '', '1435305090', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('147', 'admin', '1', '127.0.0.1', '', '1435305093', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('148', 'admin', '1', '127.0.0.1', '', '1435305096', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('149', 'admin', '1', '127.0.0.1', '', '1435305098', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('150', 'admin', '1', '127.0.0.1', '', '1435305101', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('151', 'admin', '1', '127.0.0.1', '', '1435305104', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('152', 'admin', '1', '127.0.0.1', '', '1435305106', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('153', 'admin', '1', '127.0.0.1', '', '1435305109', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('154', 'admin', '1', '127.0.0.1', '', '1435305112', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('155', 'admin', '1', '127.0.0.1', '', '1435305114', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('156', 'admin', '1', '127.0.0.1', '', '1435305144', '管理员', '删除了用户组：123');
INSERT INTO `sso_action_log` VALUES ('157', 'admin', '1', '127.0.0.1', '', '1435305194', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('158', 'admin', '1', '127.0.0.1', '', '1435543884', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('159', 'admin', '1', '127.0.0.1', '', '1435543919', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('160', 'admin', '1', '127.0.0.1', '', '1435543924', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('161', 'admin', '1', '127.0.0.1', '', '1435544846', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('162', 'admin', '1', '127.0.0.1', '', '1435544858', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('163', 'admin', '1', '127.0.0.1', '', '1435544905', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('164', 'admin', '1', '127.0.0.1', '', '1435544909', '管理员', '改变了用户组的权限：123');
INSERT INTO `sso_action_log` VALUES ('165', 'admin', '1', '127.0.0.1', '', '1435545169', '管理员', '删除了用户组：123');
INSERT INTO `sso_action_log` VALUES ('166', 'admin', '1', '127.0.0.1', '', '1435545229', '管理员', '删除了系统用户：test');
INSERT INTO `sso_action_log` VALUES ('167', 'admin', '1', '127.0.0.1', '', '1435545383', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('168', 'admin', '1', '127.0.0.1', '', '1435545965', '管理员', '改变了用户的权限：admin');
INSERT INTO `sso_action_log` VALUES ('169', 'admin', '1', '127.0.0.1', '', '1435546040', '管理员', '改变了用户的权限：admin');
INSERT INTO `sso_action_log` VALUES ('170', 'admin', '1', '127.0.0.1', '', '1435546116', '管理员', '改变了用户的权限：admin');
INSERT INTO `sso_action_log` VALUES ('171', 'admin', '1', '127.0.0.1', '', '1435546127', '管理员', '改变了用户的权限：admin');
INSERT INTO `sso_action_log` VALUES ('172', 'admin', '1', '127.0.0.1', '', '1435546171', '管理员', '改变了用户的权限：admin');
INSERT INTO `sso_action_log` VALUES ('173', 'admin', '1', '127.0.0.1', '', '1435546175', '管理员', '改变了用户的权限：admin');
INSERT INTO `sso_action_log` VALUES ('174', 'admin', '1', '127.0.0.1', '', '1435548904', '管理员', '改变了用户组的权限：超级用户组');
INSERT INTO `sso_action_log` VALUES ('175', 'admin', '1', '127.0.0.1', '', '1435551384', '管理员', '关联了用户：管理员 到工作流：测试工作流 中的审核步骤：测试1');
INSERT INTO `sso_action_log` VALUES ('176', 'admin', '1', '127.0.0.1', '', '1435551469', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('177', 'admin', '1', '127.0.0.1', '', '1435551480', '管理员', '改变了用户的权限：admin');
INSERT INTO `sso_action_log` VALUES ('178', 'admin', '1', '127.0.0.1', '', '1435551487', '管理员', '改变了用户的权限：admin');
INSERT INTO `sso_action_log` VALUES ('179', 'admin', '1', '127.0.0.1', '', '1435551491', '管理员', '改变了用户的权限：admin');
INSERT INTO `sso_action_log` VALUES ('180', 'admin', '1', '127.0.0.1', '', '1435551494', '管理员', '改变了用户的权限：admin');
INSERT INTO `sso_action_log` VALUES ('181', 'admin', '1', '127.0.0.1', '', '1435551504', '管理员', '编辑了用户组：超级用户组');
INSERT INTO `sso_action_log` VALUES ('182', 'admin', '1', '127.0.0.1', '', '1435563248', '管理员', '编辑了用户组：超级用户组');
INSERT INTO `sso_action_log` VALUES ('183', 'admin', '1', '127.0.0.1', '', '1435563266', '管理员', '编辑了用户组：超级用户组');
INSERT INTO `sso_action_log` VALUES ('184', 'admin', '1', '127.0.0.1', '', '1435563284', '管理员', '编辑了用户组：超级用户组');
INSERT INTO `sso_action_log` VALUES ('185', 'admin', '1', '127.0.0.1', '', '1435563326', '管理员', '编辑了用户组：超级用户组');
INSERT INTO `sso_action_log` VALUES ('186', 'admin', '1', '127.0.0.1', '', '1435563375', '管理员', '编辑了系统用户：admin');
INSERT INTO `sso_action_log` VALUES ('187', 'admin', '1', '127.0.0.1', '', '1435563479', '管理员', '编辑了用户组：超级用户组');
INSERT INTO `sso_action_log` VALUES ('188', 'admin', '1', '127.0.0.1', '', '1435563853', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('189', 'admin', '1', '127.0.0.1', '', '1435563866', '管理员', '改变了用户组的权限：超级用户组');
INSERT INTO `sso_action_log` VALUES ('190', 'admin', '1', '127.0.0.1', '', '1435568665', '管理员', '改变了用户组的权限：超级用户组');
INSERT INTO `sso_action_log` VALUES ('191', 'admin', '1', '127.0.0.1', '', '1435630315', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('192', 'admin', '1', '127.0.0.1', '', '1435631134', '管理员', '改变了用户的权限：admin');
INSERT INTO `sso_action_log` VALUES ('193', 'admin', '1', '127.0.0.1', '', '1435631327', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('194', 'admin', '1', '127.0.0.1', '', '1435632843', '管理员', '编辑了系统用户：admin');
INSERT INTO `sso_action_log` VALUES ('195', 'admin', '1', '127.0.0.1', '', '1435637075', '管理员', '编辑了用户组：超级用户组');
INSERT INTO `sso_action_log` VALUES ('196', 'admin', '1', '127.0.0.1', '', '1435638964', '管理员', '编辑了工作流步骤：测试1');
INSERT INTO `sso_action_log` VALUES ('197', 'admin', '1', '127.0.0.1', '', '1435649867', '管理员', '改变了用户组的权限：超级用户组');
INSERT INTO `sso_action_log` VALUES ('198', 'admin', '1', '127.0.0.1', '', '1435737120', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('199', 'admin', '1', '127.0.0.1', '', '1435738202', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('200', 'admin', '1', '127.0.0.1', '', '1436147586', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('201', 'admin', '1', '127.0.0.1', '', '1436147922', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('202', 'admin', '1', '127.0.0.1', '', '1436150023', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('203', 'admin', '1', '127.0.0.1', '', '1436150360', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('204', 'admin', '1', '127.0.0.1', '', '1436150536', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('205', 'admin', '1', '127.0.0.1', '', '1436152462', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('206', 'admin', '1', '127.0.0.1', '', '1436153673', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('207', 'admin', '1', '127.0.0.1', '', '1436153740', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('208', 'admin', '1', '127.0.0.1', '', '1436156479', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('209', 'admin', '1', '127.0.0.1', '', '1436169648', '管理员', '增加了新的系统用户：test');
INSERT INTO `sso_action_log` VALUES ('210', 'admin', '1', '127.0.0.1', '', '1436169666', '管理员', '增加了新的用户组：test');
INSERT INTO `sso_action_log` VALUES ('211', 'admin', '1', '127.0.0.1', '', '1436169678', '管理员', '编辑了系统用户：test');
INSERT INTO `sso_action_log` VALUES ('212', 'admin', '1', '127.0.0.1', '', '1436169830', '管理员', '改变了用户组的权限：test');
INSERT INTO `sso_action_log` VALUES ('213', 'admin', '1', '127.0.0.1', '', '1436169901', '管理员', '改变了用户组的权限：test');
INSERT INTO `sso_action_log` VALUES ('214', 'admin', '1', '127.0.0.1', '', '1436169909', '管理员', '改变了用户组的权限：test');
INSERT INTO `sso_action_log` VALUES ('215', 'test', '2', '127.0.0.1', '', '1436169949', 'test', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('216', 'admin', '1', '127.0.0.1', '', '1436170007', '管理员', '改变了用户组的权限：test');
INSERT INTO `sso_action_log` VALUES ('217', 'test', '2', '127.0.0.1', '', '1436170015', 'test', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('218', 'admin', '1', '127.0.0.1', '', '1436170400', '管理员', '改变了用户组的权限：test');
INSERT INTO `sso_action_log` VALUES ('219', 'test', '2', '127.0.0.1', '', '1436170410', 'test', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('220', 'admin', '1', '127.0.0.1', '', '1436170781', '管理员', '改变了用户组的权限：test');
INSERT INTO `sso_action_log` VALUES ('221', 'test', '2', '127.0.0.1', '', '1436170792', 'test', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('222', 'test', '2', '127.0.0.1', '', '1436170892', 'test', '增加了新的用户组：test2');
INSERT INTO `sso_action_log` VALUES ('223', 'test', '2', '127.0.0.1', '', '1436170910', 'test', '增加了新的系统用户：test2');
INSERT INTO `sso_action_log` VALUES ('224', 'test', '2', '127.0.0.1', '', '1436171238', 'test', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('225', 'test', '2', '127.0.0.1', '', '1436171403', 'test', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('226', 'admin', '1', '127.0.0.1', '', '1436232722', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('227', 'admin', '1', '127.0.0.1', '', '1436237053', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('228', 'admin', '1', '127.0.0.1', '', '1436237105', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('229', 'test', '2', '127.0.0.1', '', '1436237162', 'test', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('230', 'admin', '1', '127.0.0.1', '', '1436237329', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('231', 'admin', '1', '127.0.0.1', '', '1436237524', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('232', 'admin', '1', '127.0.0.1', '', '1436237637', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('233', 'admin', '1', '127.0.0.1', '', '1436237837', '管理员', '改变了用户组的权限：test');
INSERT INTO `sso_action_log` VALUES ('234', 'test', '2', '127.0.0.1', '', '1436237855', 'test', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('235', 'admin', '1', '127.0.0.1', '', '1436237898', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('236', 'admin', '1', '127.0.0.1', '', '1436237901', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('237', 'admin', '1', '127.0.0.1', '', '1436237909', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('238', 'admin', '1', '127.0.0.1', '', '1436238450', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('239', 'admin', '1', '127.0.0.1', '', '1436238456', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('240', 'admin', '1', '127.0.0.1', '', '1436238502', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('241', 'admin', '1', '127.0.0.1', '', '1436238870', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('242', 'admin', '1', '127.0.0.1', '', '1436238871', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('243', 'admin', '1', '127.0.0.1', '', '1436239312', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('244', 'admin', '1', '127.0.0.1', '', '1436239313', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('245', 'admin', '1', '127.0.0.1', '', '1436239654', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('246', 'admin', '1', '127.0.0.1', '', '1436239658', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('247', 'admin', '1', '127.0.0.1', '', '1436239716', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('248', 'admin', '1', '127.0.0.1', '', '1436239767', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('249', 'admin', '1', '127.0.0.1', '', '1436239771', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('250', 'admin', '1', '127.0.0.1', '', '1436240071', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('251', 'admin', '1', '127.0.0.1', '', '1436240359', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('252', 'admin', '1', '127.0.0.1', '', '1436240365', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('253', 'admin', '1', '127.0.0.1', '', '1436240399', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('254', 'admin', '1', '127.0.0.1', '', '1436240488', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('255', 'admin', '1', '127.0.0.1', '', '1436240505', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('256', 'admin', '1', '127.0.0.1', '', '1436240705', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('257', 'admin', '1', '127.0.0.1', '', '1436240712', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('258', 'admin', '1', '127.0.0.1', '', '1436240856', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('259', 'admin', '1', '127.0.0.1', '', '1436241021', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('260', 'admin', '1', '127.0.0.1', '', '1436241046', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('261', 'admin', '1', '127.0.0.1', '', '1436241056', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('262', 'admin', '1', '127.0.0.1', '', '1436241060', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('263', 'admin', '1', '127.0.0.1', '', '1436241060', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('264', 'admin', '1', '127.0.0.1', '', '1436241253', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('265', 'admin', '1', '127.0.0.1', '', '1436241261', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('266', 'admin', '1', '127.0.0.1', '', '1436241264', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('267', 'admin', '1', '127.0.0.1', '', '1436241311', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('268', 'admin', '1', '127.0.0.1', '', '1436241317', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('269', 'admin', '1', '127.0.0.1', '', '1436241322', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('270', 'admin', '1', '127.0.0.1', '', '1436241338', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('271', 'admin', '1', '127.0.0.1', '', '1436241435', '管理员', '编辑了用户组：test2');
INSERT INTO `sso_action_log` VALUES ('272', 'admin', '1', '127.0.0.1', '', '1436241449', '管理员', '改变了用户组的权限：test2');
INSERT INTO `sso_action_log` VALUES ('273', 'admin', '1', '127.0.0.1', '', '1436241533', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('274', 'test', '2', '127.0.0.1', '', '1436241545', 'test', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('275', 'test', '2', '127.0.0.1', '', '1436241556', 'test', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('276', 'test', '2', '127.0.0.1', '', '1436241995', 'test', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('277', 'admin', '1', '127.0.0.1', '', '1436242006', '管理员', '改变了用户组的权限：test2');
INSERT INTO `sso_action_log` VALUES ('278', 'admin', '1', '127.0.0.1', '', '1436242054', '管理员', '改变了用户组的权限：test2');
INSERT INTO `sso_action_log` VALUES ('279', 'admin', '1', '127.0.0.1', '', '1436242083', '管理员', '改变了用户组的权限：test2');
INSERT INTO `sso_action_log` VALUES ('280', 'admin', '1', '127.0.0.1', '', '1436242180', '管理员', '改变了用户组的权限：test2');
INSERT INTO `sso_action_log` VALUES ('281', 'admin', '1', '127.0.0.1', '', '1436242954', '管理员', '改变了用户组的权限：test2');
INSERT INTO `sso_action_log` VALUES ('282', 'admin', '1', '127.0.0.1', '', '1436242957', '管理员', '改变了用户组的权限：test2');
INSERT INTO `sso_action_log` VALUES ('283', 'admin', '1', '127.0.0.1', '', '1436242960', '管理员', '改变了用户组的权限：test2');
INSERT INTO `sso_action_log` VALUES ('284', 'admin', '1', '127.0.0.1', '', '1436242963', '管理员', '改变了用户组的权限：test2');
INSERT INTO `sso_action_log` VALUES ('285', 'admin', '1', '127.0.0.1', '', '1436242986', '管理员', '改变了用户组的权限：test2');
INSERT INTO `sso_action_log` VALUES ('286', 'admin', '1', '127.0.0.1', '', '1436242989', '管理员', '改变了用户组的权限：test2');
INSERT INTO `sso_action_log` VALUES ('287', 'admin', '1', '127.0.0.1', '', '1436244454', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('288', 'admin', '1', '127.0.0.1', '', '1436245828', '管理员', '编辑了用户组：test2');
INSERT INTO `sso_action_log` VALUES ('289', 'admin', '1', '127.0.0.1', '', '1436246346', '管理员', '删除了用户组：test2');
INSERT INTO `sso_action_log` VALUES ('290', 'admin', '1', '127.0.0.1', '', '1436246373', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('291', 'admin', '1', '127.0.0.1', '', '1436247233', '管理员', '改变了用户组的权限：test');
INSERT INTO `sso_action_log` VALUES ('292', 'admin', '1', '127.0.0.1', '', '1436321211', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('293', 'admin', '1', '127.0.0.1', '', '1436325559', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('294', 'admin', '1', '127.0.0.1', '', '1436499013', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('295', 'admin', '1', '127.0.0.1', '', '1436500439', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('296', 'admin', '1', '127.0.0.1', '', '1436501830', '管理员', '增加了新的工作流：测试辅助权限工作流');
INSERT INTO `sso_action_log` VALUES ('297', 'admin', '1', '127.0.0.1', '', '1436501908', '管理员', '编辑了工作流：测试辅助权限工作流');
INSERT INTO `sso_action_log` VALUES ('298', 'admin', '1', '127.0.0.1', '', '1436504505', '管理员', '增加了新的工作流步骤：测试辅助权限工作流步骤');
INSERT INTO `sso_action_log` VALUES ('299', 'admin', '1', '127.0.0.1', '', '1436504716', '管理员', '编辑了工作流步骤：测试辅助权限工作流步骤');
INSERT INTO `sso_action_log` VALUES ('300', 'admin', '1', '127.0.0.1', '', '1436504733', '管理员', '增加了新的工作流步骤：123');
INSERT INTO `sso_action_log` VALUES ('301', 'admin', '1', '127.0.0.1', '', '1436504748', '管理员', '编辑了工作流步骤：123');
INSERT INTO `sso_action_log` VALUES ('302', 'admin', '1', '127.0.0.1', '', '1436504788', '管理员', '增加了新的工作流步骤：测试辅助权限工作流步骤2');
INSERT INTO `sso_action_log` VALUES ('303', 'admin', '1', '127.0.0.1', '', '1436504811', '管理员', '编辑了工作流步骤：测试辅助权限工作流步骤2');
INSERT INTO `sso_action_log` VALUES ('304', 'admin', '1', '127.0.0.1', '', '1436504822', '管理员', '编辑了工作流步骤：测试辅助权限工作流步骤2');
INSERT INTO `sso_action_log` VALUES ('305', 'admin', '1', '127.0.0.1', '', '1436504830', '管理员', '编辑了工作流步骤：123');
INSERT INTO `sso_action_log` VALUES ('306', 'admin', '1', '127.0.0.1', '', '1436504841', '管理员', '增加了新的工作流步骤：123123gg');
INSERT INTO `sso_action_log` VALUES ('307', 'admin', '1', '127.0.0.1', '', '1436504850', '管理员', '编辑了工作流步骤：123123gg');
INSERT INTO `sso_action_log` VALUES ('308', 'admin', '1', '127.0.0.1', '', '1436504859', '管理员', '删除了工作流步骤：123');
INSERT INTO `sso_action_log` VALUES ('309', 'admin', '1', '127.0.0.1', '', '1436504863', '管理员', '删除了工作流步骤：123123gg');
INSERT INTO `sso_action_log` VALUES ('310', 'admin', '1', '127.0.0.1', '', '1436504877', '管理员', '关联了用户：test 到工作流：测试辅助权限工作流 中的审核步骤：测试辅助权限工作流步骤');
INSERT INTO `sso_action_log` VALUES ('311', 'admin', '1', '127.0.0.1', '', '1436504877', '管理员', '关联了用户：管理员 到工作流：测试辅助权限工作流 中的审核步骤：测试辅助权限工作流步骤');
INSERT INTO `sso_action_log` VALUES ('312', 'admin', '1', '127.0.0.1', '', '1436504889', '管理员', '关联了用户：1 到工作流：测试辅助权限工作流 中的审核步骤：测试辅助权限工作流步骤2');
INSERT INTO `sso_action_log` VALUES ('313', 'admin', '1', '127.0.0.1', '', '1436509226', '管理员', '关联了用户：1 到工作流：测试工作流 中的审核步骤：测试2');
INSERT INTO `sso_action_log` VALUES ('314', 'admin', '1', '127.0.0.1', '', '1436510712', '管理员', '增加了新的工作流：辅助权限测试2');
INSERT INTO `sso_action_log` VALUES ('315', 'admin', '1', '127.0.0.1', '', '1436510726', '管理员', '编辑了工作流：辅助权限测试2');
INSERT INTO `sso_action_log` VALUES ('316', 'admin', '1', '127.0.0.1', '', '1436510735', '管理员', '编辑了工作流：辅助权限测试2');
INSERT INTO `sso_action_log` VALUES ('317', 'admin', '1', '127.0.0.1', '', '1436510752', '管理员', '删除了工作流：测试辅助权限工作流');
INSERT INTO `sso_action_log` VALUES ('318', 'admin', '1', '127.0.0.1', '', '1436510782', '管理员', '增加了新的工作流步骤：测试辅助权限工作流步骤');
INSERT INTO `sso_action_log` VALUES ('319', 'admin', '1', '127.0.0.1', '', '1436510790', '管理员', '编辑了工作流步骤：测试辅助权限工作流步骤');
INSERT INTO `sso_action_log` VALUES ('320', 'admin', '1', '127.0.0.1', '', '1436510821', '管理员', '增加了新的工作流步骤：234');
INSERT INTO `sso_action_log` VALUES ('321', 'admin', '1', '127.0.0.1', '', '1436510828', '管理员', '删除了工作流步骤：234');
INSERT INTO `sso_action_log` VALUES ('322', 'admin', '1', '127.0.0.1', '', '1436510854', '管理员', '关联了用户：test 到工作流：辅助权限测试2 中的审核步骤：测试辅助权限工作流步骤');
INSERT INTO `sso_action_log` VALUES ('323', 'admin', '1', '127.0.0.1', '', '1436510871', '管理员', '删除了工作流：工作流1');
INSERT INTO `sso_action_log` VALUES ('324', 'admin', '1', '127.0.0.1', '', '1436510894', '管理员', '增加了新的工作流：123123ffffffff');
INSERT INTO `sso_action_log` VALUES ('325', 'admin', '1', '127.0.0.1', '', '1436510906', '管理员', '编辑了工作流：123123ffffffff');
INSERT INTO `sso_action_log` VALUES ('326', 'admin', '1', '127.0.0.1', '', '1436510918', '管理员', '增加了新的工作流步骤：123123');
INSERT INTO `sso_action_log` VALUES ('327', 'admin', '1', '127.0.0.1', '', '1436510925', '管理员', '编辑了工作流步骤：123123');
INSERT INTO `sso_action_log` VALUES ('328', 'admin', '1', '127.0.0.1', '', '1436510933', '管理员', '关联了用户：管理员 到工作流：123123ffffffff 中的审核步骤：123123');
INSERT INTO `sso_action_log` VALUES ('329', 'admin', '1', '127.0.0.1', '', '1436510945', '管理员', '删除了工作流：123123ffffffff');
INSERT INTO `sso_action_log` VALUES ('330', 'admin', '1', '127.0.0.1', '', '1439442301', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('331', 'admin', '1', '127.0.0.1', '', '1439442348', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('332', 'admin', '1', '127.0.0.1', '', '1439865623', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('333', 'admin', '1', '127.0.0.1', '', '1439866327', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('334', 'admin', '1', '127.0.0.1', '', '1444790367', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('335', 'admin', '1', '127.0.0.1', '', '1444790383', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('336', 'admin', '1', '127.0.0.1', '', '1444790425', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('337', 'admin', '1', '127.0.0.1', '', '1444790433', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('338', 'admin', '1', '127.0.0.1', '', '1444790493', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('339', 'admin', '1', '127.0.0.1', '', '1444790565', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('340', 'admin', '1', '127.0.0.1', '', '1444790671', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('341', 'admin', '1', '127.0.0.1', '', '1444790883', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('342', 'admin', '1', '127.0.0.1', '', '1444790902', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('343', 'admin', '1', '127.0.0.1', '', '1444804379', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('344', 'admin', '1', '127.0.0.1', '', '1444804473', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('345', 'admin', '1', '127.0.0.1', '', '1444810810', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('346', 'admin', '1', '127.0.0.1', '', '1444810824', '管理员', '编辑了工作流步骤：测试辅助权限工作流步骤');
INSERT INTO `sso_action_log` VALUES ('347', 'admin', '1', '127.0.0.1', '', '1444810828', '管理员', '关联了用户：test 到工作流：辅助权限测试2 中的审核步骤：测试辅助权限工作流步骤');
INSERT INTO `sso_action_log` VALUES ('348', 'admin', '1', '127.0.0.1', '', '1444810828', '管理员', '关联了用户：管理员 到工作流：辅助权限测试2 中的审核步骤：测试辅助权限工作流步骤');
INSERT INTO `sso_action_log` VALUES ('349', 'admin', '1', '127.0.0.1', '', '1444810883', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('350', 'admin', '1', '127.0.0.1', '', '1444889723', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('351', 'admin', '1', '127.0.0.1', '', '1444891417', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('352', 'admin', '1', '127.0.0.1', '', '1444893647', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('353', 'admin', '1', '127.0.0.1', '', '1444898279', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('354', 'admin', '1', '127.0.0.1', '', '1444898483', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('355', 'admin', '1', '127.0.0.1', '', '1444898570', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('356', 'admin', '1', '127.0.0.1', '', '1444898638', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('357', 'admin', '1', '127.0.0.1', '', '1444957433', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('358', 'admin', '1', '127.0.0.1', '', '1444957755', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('359', 'admin', '1', '127.0.0.1', '', '1444958561', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('360', 'admin', '1', '127.0.0.1', '', '1445311288', '管理员', '登录系统成功');
INSERT INTO `sso_action_log` VALUES ('361', 'admin', '1', '127.0.0.1', '', '1445311773', '管理员', '编辑了系统用户：test2');
INSERT INTO `sso_action_log` VALUES ('362', 'admin', '1', '127.0.0.1', '', '1445311812', '管理员', '改变了用户的权限：test2');
INSERT INTO `sso_action_log` VALUES ('363', 'admin', '1', '127.0.0.1', '', '1445311825', '管理员', '登录系统成功');

-- ----------------------------
-- Table structure for `sso_group`
-- ----------------------------
DROP TABLE IF EXISTS `sso_group`;
CREATE TABLE `sso_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) NOT NULL COMMENT '用户组名',
  `mark` varchar(255) NOT NULL COMMENT '备注',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否禁用',
  `level` int(11) NOT NULL DEFAULT '0' COMMENT '用户组等级，低等级的不能对高等级的用户做修改',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户组表_by_jiang';

-- ----------------------------
-- Records of sso_group
-- ----------------------------
INSERT INTO `sso_group` VALUES ('1', '超级用户组', '123123a', '1', '1');
INSERT INTO `sso_group` VALUES ('2', 'test', '1', '1', '1');

-- ----------------------------
-- Table structure for `sso_members`
-- ----------------------------
DROP TABLE IF EXISTS `sso_members`;
CREATE TABLE `sso_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '用户密码',
  `group_id` int(11) NOT NULL,
  `realname` varchar(255) NOT NULL DEFAULT '' COMMENT '真实性名',
  `token` varchar(255) NOT NULL COMMENT '用户注册时的密钥',
  `add_time` bigint(20) NOT NULL COMMENT '用户注册的时间',
  `modify_time` bigint(20) NOT NULL COMMENT '用户信息所修改的时间',
  `mobile` varchar(11) NOT NULL COMMENT '手机',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户禁用0正常的1',
  `mark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `last_login_ip` varchar(255) NOT NULL COMMENT '最后登录ip',
  `last_login_time` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE,
  KEY `password` (`password`) USING BTREE,
  KEY `group_id` (`group_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='用户表_by_jiang';

-- ----------------------------
-- Records of sso_members
-- ----------------------------
INSERT INTO `sso_members` VALUES ('1', 'admin', '6512bd43d9caa6e02c990b0a82652dca', '1', '管理员', 'oyzzO7YxmgJHlAfdK5HaZMscegJPcTrw5drPQRS6bjlfAkTB6NELPvqpc12q', '0', '0', '12', '1', '超级用户组', '127.0.0.1', '1445221763');
INSERT INTO `sso_members` VALUES ('2', 'test', 'c4ca4238a0b923820dcc509a6f75849b', '2', 'test', '', '1436169648', '0', '11', '1', '1', '127.0.0.1', '1436241545');
INSERT INTO `sso_members` VALUES ('3', 'test2', 'c4ca4238a0b923820dcc509a6f75849b', '3', '1', '', '1436170910', '0', '1', '1', '1', '', '0');
INSERT INTO `sso_members` VALUES ('4', 'test1', '96e79218965eb72c92a549dd5a330112', '0', 'imya', '', '0', '0', '', '1', '', '', '0');
INSERT INTO `sso_members` VALUES ('5', 'test11', '96e79218965eb72c92a549dd5a330112', '0', 'imya', '', '0', '0', '', '1', '', '', '0');
INSERT INTO `sso_members` VALUES ('6', 'wo', '6512bd43d9caa6e02c990b0a82652dca', '0', 'wowowowo', '', '0', '0', '', '1', '', '127.0.0.1', '1445311268');
INSERT INTO `sso_members` VALUES ('7', 'woo', '6512bd43d9caa6e02c990b0a82652dca', '0', 'woo', '', '0', '0', '', '1', '', '', '0');
INSERT INTO `sso_members` VALUES ('8', 'hi', 'c4ca4238a0b923820dcc509a6f75849b', '0', 'hiii', '', '0', '0', '', '1', '', '', '0');
INSERT INTO `sso_members` VALUES ('9', 'kk', 'c4ca4238a0b923820dcc509a6f75849b', '0', 'kk', '', '0', '0', '', '1', '', '127.0.0.1', '1445313078');

-- ----------------------------
-- Table structure for `sso_oauth_access_token_scopes`
-- ----------------------------
DROP TABLE IF EXISTS `sso_oauth_access_token_scopes`;
CREATE TABLE `sso_oauth_access_token_scopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `access_token_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `oauth_access_token_scopes_access_token_id_index` (`access_token_id`),
  KEY `oauth_access_token_scopes_scope_id_index` (`scope_id`),
  CONSTRAINT `oauth_access_token_scopes_access_token_id_foreign` FOREIGN KEY (`access_token_id`) REFERENCES `sso_oauth_access_tokens` (`id`) ON DELETE CASCADE,
  CONSTRAINT `oauth_access_token_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `sso_oauth_scopes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sso_oauth_access_token_scopes
-- ----------------------------

-- ----------------------------
-- Table structure for `sso_oauth_access_tokens`
-- ----------------------------
DROP TABLE IF EXISTS `sso_oauth_access_tokens`;
CREATE TABLE `sso_oauth_access_tokens` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` int(10) unsigned NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth_access_tokens_id_session_id_unique` (`id`,`session_id`),
  KEY `oauth_access_tokens_session_id_index` (`session_id`),
  CONSTRAINT `oauth_access_tokens_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sso_oauth_sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sso_oauth_access_tokens
-- ----------------------------
INSERT INTO `sso_oauth_access_tokens` VALUES ('0nQipUHlRzS8pN6C5aRpE9jbCwLoqgy0Y0Fs0HE3', '53', '1444902190', '2015-10-15 08:43:10', '2015-10-15 08:43:10');
INSERT INTO `sso_oauth_access_tokens` VALUES ('1CGtzKEQH7WBVLcLzyFjhEAznzvTRi0nQVYxFpjB', '28', '1444890746', '2015-10-15 05:32:26', '2015-10-15 05:32:26');
INSERT INTO `sso_oauth_access_tokens` VALUES ('2OtSPeQWaknuABnMQNrhjtzpzgS2uOkkm2YaylSN', '42', '1444901576', '2015-10-15 08:32:56', '2015-10-15 08:32:56');
INSERT INTO `sso_oauth_access_tokens` VALUES ('3dwahialtLt11onnormgIrL76mx9aSVdHR1821sQ', '37', '1444892869', '2015-10-15 06:07:49', '2015-10-15 06:07:49');
INSERT INTO `sso_oauth_access_tokens` VALUES ('3LunelKFojNiTqgjkaaaXsrVktbHzonPDP2nT8G1', '76', '1445225364', '2015-10-19 02:29:24', '2015-10-19 02:29:24');
INSERT INTO `sso_oauth_access_tokens` VALUES ('4g3bQATfxEQvTSNBdambznFPF8N7s9IBcqwSJh6C', '29', '1444891713', '2015-10-15 05:48:33', '2015-10-15 05:48:33');
INSERT INTO `sso_oauth_access_tokens` VALUES ('52xD0QZAIU21zQtcmuHqBPeDNvNZdqJHMaUd6z08', '55', '1444902280', '2015-10-15 08:44:40', '2015-10-15 08:44:40');
INSERT INTO `sso_oauth_access_tokens` VALUES ('7xsCp24Xl0nvYpEpnV8YqDgR4yG7Fp4Eq9OTHqCY', '30', '1444891735', '2015-10-15 05:48:55', '2015-10-15 05:48:55');
INSERT INTO `sso_oauth_access_tokens` VALUES ('9I8NN2I0hwCXzMNSa10SsE2VdAfcvgWZAHxfS3Bt', '50', '1444901846', '2015-10-15 08:37:26', '2015-10-15 08:37:26');
INSERT INTO `sso_oauth_access_tokens` VALUES ('A1UVxcRtXNiCGP4k9XjQwaZ3KppXicvEwAK6Gzi8', '35', '1444892779', '2015-10-15 06:06:19', '2015-10-15 06:06:19');
INSERT INTO `sso_oauth_access_tokens` VALUES ('ADbAxA4WUI7HTi8oyLdQfFreEGVoNX37Q3NlVcXs', '71', '1444966165', '2015-10-16 02:29:25', '2015-10-16 02:29:25');
INSERT INTO `sso_oauth_access_tokens` VALUES ('aRMTTufLW6KFAtKRzA6wWzlln5rGOD1kcR0bi9tR', '38', '1444892979', '2015-10-15 06:09:39', '2015-10-15 06:09:39');
INSERT INTO `sso_oauth_access_tokens` VALUES ('AS13vVSPwLRHYkdnHIXHKiEIVBN4HBkv6OxICCxy', '44', '1444901588', '2015-10-15 08:33:08', '2015-10-15 08:33:08');
INSERT INTO `sso_oauth_access_tokens` VALUES ('At82pc3zS5MDdNw52J9cq5VypqKKugfbewMetjdd', '19', '1444815810', '2015-10-14 08:43:30', '2015-10-14 08:43:30');
INSERT INTO `sso_oauth_access_tokens` VALUES ('B67njqQWVIlZTmlnwlUxkTvN3D9oLZvqp4A6meWv', '18', '1444815707', '2015-10-14 08:41:47', '2015-10-14 08:41:47');
INSERT INTO `sso_oauth_access_tokens` VALUES ('BaxNNgcmKW7x1Yd4xepHreDyaXjLyZzhnvvawP0z', '22', '1444817255', '2015-10-14 09:07:35', '2015-10-14 09:07:35');
INSERT INTO `sso_oauth_access_tokens` VALUES ('bs8BuvKvI4IUg0IHy7ho4iCCoIyX7ghUcMbRGRzn', '79', '1445236577', '2015-10-19 05:36:17', '2015-10-19 05:36:17');
INSERT INTO `sso_oauth_access_tokens` VALUES ('D57PU9OX8Pl3MHRMWCmBjeTxrioMEl0j89NNEOdo', '87', '1445316658', '2015-10-20 03:50:58', '2015-10-20 03:50:58');
INSERT INTO `sso_oauth_access_tokens` VALUES ('DB8oKc6fJKJknuOUwwOMp4ipO35fwGJAEGWTjQWo', '75', '1444966419', '2015-10-16 02:33:39', '2015-10-16 02:33:39');
INSERT INTO `sso_oauth_access_tokens` VALUES ('dCbyvXn7KOEVguyUgekwUXrx2E6LhJxrdZoGnXI6', '73', '1444966234', '2015-10-16 02:30:34', '2015-10-16 02:30:34');
INSERT INTO `sso_oauth_access_tokens` VALUES ('DnYkCy1DPc8DEehzYu4u0Mok6qSdDUgV0pdsxY27', '72', '1444966170', '2015-10-16 02:29:30', '2015-10-16 02:29:30');
INSERT INTO `sso_oauth_access_tokens` VALUES ('efQdr8dNp2ScBZCxia13QVC15C1e5JXbO71TKKml', '88', '1445316678', '2015-10-20 03:51:18', '2015-10-20 03:51:18');
INSERT INTO `sso_oauth_access_tokens` VALUES ('EOMfyMwLmg12b2VOnOwsD5x01OkrkOdJyOmG5O37', '74', '1444966408', '2015-10-16 02:33:28', '2015-10-16 02:33:28');
INSERT INTO `sso_oauth_access_tokens` VALUES ('eRYW57LAm9yRHSI72x7SwkU8w9UGt2HthUwiL1Gc', '51', '1444902063', '2015-10-15 08:41:03', '2015-10-15 08:41:03');
INSERT INTO `sso_oauth_access_tokens` VALUES ('EZU8qW1w0TeSkBhyFVaehpsrZba6lwiZmqq9OLEn', '86', '1445315970', '2015-10-20 03:39:30', '2015-10-20 03:39:30');
INSERT INTO `sso_oauth_access_tokens` VALUES ('fCxM6oB6xwssmJ6pjI0VpDgAjqRf4Ftyqnf5oqGa', '33', '1444892686', '2015-10-15 06:04:46', '2015-10-15 06:04:46');
INSERT INTO `sso_oauth_access_tokens` VALUES ('fNBoy9Cj8VeCag149okqCCRPLkS3aXLfL8kxMWop', '52', '1444902160', '2015-10-15 08:42:40', '2015-10-15 08:42:40');
INSERT INTO `sso_oauth_access_tokens` VALUES ('gp7oKGH47YwHrqlkdVxvTt8dZjnWl8ZwL0Z0B30g', '21', '1444817171', '2015-10-14 09:06:11', '2015-10-14 09:06:11');
INSERT INTO `sso_oauth_access_tokens` VALUES ('H49bggKKakR5rqP0HmO3y33hpnAqAIMF1rs57XZ7', '20', '1444817084', '2015-10-14 09:04:44', '2015-10-14 09:04:44');
INSERT INTO `sso_oauth_access_tokens` VALUES ('h9LZnIA0oBODxTUoEuCY0EQji6zizWHrmmELbbVN', '27', '1444888956', '2015-10-15 05:02:36', '2015-10-15 05:02:36');
INSERT INTO `sso_oauth_access_tokens` VALUES ('hJpPBetS48LK0eXgtgYJqbSLY1bruwGdmW38GLo0', '77', '1445225373', '2015-10-19 02:29:33', '2015-10-19 02:29:33');
INSERT INTO `sso_oauth_access_tokens` VALUES ('i4IRUPtl7zd6ps63IbmJ6KoufZHhi6GSKsD8OjtZ', '85', '1445314868', '2015-10-20 03:21:08', '2015-10-20 03:21:08');
INSERT INTO `sso_oauth_access_tokens` VALUES ('iEtEw1WXVveyifVSiMiRu2X61mb3mOQJe9dLSJuQ', '80', '1445236594', '2015-10-19 05:36:34', '2015-10-19 05:36:34');
INSERT INTO `sso_oauth_access_tokens` VALUES ('IFiAaxW7gNz95kkkmdYp2aCwIKxtAjY4cjyffw0U', '34', '1444892756', '2015-10-15 06:05:56', '2015-10-15 06:05:56');
INSERT INTO `sso_oauth_access_tokens` VALUES ('k3oTzTnSSEa2HFpfvVphc2qDBH6riaL4KuDp48Dw', '58', '1444903366', '2015-10-15 09:02:46', '2015-10-15 09:02:46');
INSERT INTO `sso_oauth_access_tokens` VALUES ('M7DruhSXwyF7pYAjrcy8V9ESosd53xF9qtLRBeEq', '47', '1444901749', '2015-10-15 08:35:49', '2015-10-15 08:35:49');
INSERT INTO `sso_oauth_access_tokens` VALUES ('n1kB2rZ7jkvxrtYF7BJbzV6R2zbm6y8UJMPKRmKM', '83', '1445314815', '2015-10-20 03:20:15', '2015-10-20 03:20:15');
INSERT INTO `sso_oauth_access_tokens` VALUES ('namQsmaRdrNYoXIUXMUnbFi6VpZRg2HNkB8ZK0Bl', '84', '1445314842', '2015-10-20 03:20:42', '2015-10-20 03:20:42');
INSERT INTO `sso_oauth_access_tokens` VALUES ('nwf1DlCBO9un1RRfNa7oMh8sxK9vOyrB4TZOQNgj', '32', '1444892179', '2015-10-15 05:56:19', '2015-10-15 05:56:19');
INSERT INTO `sso_oauth_access_tokens` VALUES ('oSoHNJvCEH05ePWZL5qcHMIc0x2DkFKTBA44fX9U', '59', '1444903423', '2015-10-15 09:03:43', '2015-10-15 09:03:43');
INSERT INTO `sso_oauth_access_tokens` VALUES ('OvpsVDepb8JC4LXDCLxPXtQkKQkbWJRJhA0QChvd', '68', '1444964652', '2015-10-16 02:04:12', '2015-10-16 02:04:12');
INSERT INTO `sso_oauth_access_tokens` VALUES ('Pmu7nVTE5Kb3mvUPgH1KlL3U2hGEo4vLLDKgWfup', '54', '1444902261', '2015-10-15 08:44:21', '2015-10-15 08:44:21');
INSERT INTO `sso_oauth_access_tokens` VALUES ('Q74pl4oUstl2T2SG26whwTiaUxMXi7dK8pgqMYSW', '49', '1444901821', '2015-10-15 08:37:01', '2015-10-15 08:37:01');
INSERT INTO `sso_oauth_access_tokens` VALUES ('QXGXpjomEKJtRm73mppWTnnbbGEa1Anfa2omRnRd', '56', '1444903035', '2015-10-15 08:57:15', '2015-10-15 08:57:15');
INSERT INTO `sso_oauth_access_tokens` VALUES ('Rf6Yx88fcRaAlKYku7Kfn2bNu6cJ773DvsN5eFd0', '82', '1445309672', '2015-10-20 01:54:32', '2015-10-20 01:54:32');
INSERT INTO `sso_oauth_access_tokens` VALUES ('rjODXV0qa3OhEHozACA8BpS92z9y6YhiMNzTlYzh', '23', '1444817450', '2015-10-14 09:10:50', '2015-10-14 09:10:50');
INSERT INTO `sso_oauth_access_tokens` VALUES ('TevAWjAAzg1Jb4CO1kxnmIq6YlrrkLebBPI954aD', '57', '1444903353', '2015-10-15 09:02:33', '2015-10-15 09:02:33');
INSERT INTO `sso_oauth_access_tokens` VALUES ('TKyKXqjLrqTsmzdnC82ruDwikKZfkSF2qXV7Ssqu', '48', '1444901812', '2015-10-15 08:36:52', '2015-10-15 08:36:52');
INSERT INTO `sso_oauth_access_tokens` VALUES ('TYT2jwEFQ7bD6M6lhxk9cRROPZZIctL31pjvu7o6', '40', '1444893188', '2015-10-15 06:13:08', '2015-10-15 06:13:08');
INSERT INTO `sso_oauth_access_tokens` VALUES ('U4RrTQTTCJO1Ixbi54AGzsnFsJ5xpZzPQBWRep7D', '39', '1444893116', '2015-10-15 06:11:56', '2015-10-15 06:11:56');
INSERT INTO `sso_oauth_access_tokens` VALUES ('UP2scaIK8UZtPhPfHs1RAj8Gb0AUP2DVJ6a7rsxE', '78', '1445236513', '2015-10-19 05:35:13', '2015-10-19 05:35:13');
INSERT INTO `sso_oauth_access_tokens` VALUES ('uRvKb6878A58pe57aezKkQTylZgU8ZzD2TTKNNW8', '31', '1444892067', '2015-10-15 05:54:27', '2015-10-15 05:54:27');
INSERT INTO `sso_oauth_access_tokens` VALUES ('V6KlJHWyWpHqKKtGSoR5FmMa3YnGnbDMjTvY8KuH', '25', '1444875797', '2015-10-15 01:23:17', '2015-10-15 01:23:17');
INSERT INTO `sso_oauth_access_tokens` VALUES ('VLQ0jKiRV8WYYIZy0c91tt7I1S8f7MYeM38dmkjK', '17', '1439867117', '2015-08-18 02:05:17', '2015-08-18 02:05:17');
INSERT INTO `sso_oauth_access_tokens` VALUES ('Vq5A8JJJN8MTHSNKYSob59r6SefmwScioPKoqOky', '36', '1444892851', '2015-10-15 06:07:31', '2015-10-15 06:07:31');
INSERT INTO `sso_oauth_access_tokens` VALUES ('VY7SudXwNS5kz2xiFu1DFITMAADOGMdXI8jBW1TV', '26', '1444876334', '2015-10-15 01:32:14', '2015-10-15 01:32:14');
INSERT INTO `sso_oauth_access_tokens` VALUES ('wkusGvmo8KTjsjpBxiJtB4vEJzZuNqSWlPYYdC7x', '24', '1444817679', '2015-10-14 09:14:39', '2015-10-14 09:14:39');
INSERT INTO `sso_oauth_access_tokens` VALUES ('yc2i1DZZ1BZc1aSxQlTpXngEDdjAjXbLnW0860wV', '43', '1444901580', '2015-10-15 08:33:00', '2015-10-15 08:33:00');
INSERT INTO `sso_oauth_access_tokens` VALUES ('YoqdvDeXn3invB0AjSH8jTbVrSy3f6xWGu5lMEXj', '46', '1444901744', '2015-10-15 08:35:44', '2015-10-15 08:35:44');
INSERT INTO `sso_oauth_access_tokens` VALUES ('YtxM1GIAF7nwWO4O6gM5gNG9qvtcwCsYMWSlm7UH', '41', '1444901485', '2015-10-15 08:31:25', '2015-10-15 08:31:25');
INSERT INTO `sso_oauth_access_tokens` VALUES ('YY1J9R7WnYFUXbuXWYeiaRmeIYSEceVhq6np2EUx', '81', '1445306419', '2015-10-20 01:00:19', '2015-10-20 01:00:19');
INSERT INTO `sso_oauth_access_tokens` VALUES ('ZspXYew3iNlzFc0wuf40Hso5C0Pftq9QuLo0a85h', '45', '1444901644', '2015-10-15 08:34:04', '2015-10-15 08:34:04');

-- ----------------------------
-- Table structure for `sso_oauth_auth_code_scopes`
-- ----------------------------
DROP TABLE IF EXISTS `sso_oauth_auth_code_scopes`;
CREATE TABLE `sso_oauth_auth_code_scopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `auth_code_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `oauth_auth_code_scopes_auth_code_id_index` (`auth_code_id`),
  KEY `oauth_auth_code_scopes_scope_id_index` (`scope_id`),
  CONSTRAINT `oauth_auth_code_scopes_auth_code_id_foreign` FOREIGN KEY (`auth_code_id`) REFERENCES `sso_oauth_auth_codes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `oauth_auth_code_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `sso_oauth_scopes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sso_oauth_auth_code_scopes
-- ----------------------------

-- ----------------------------
-- Table structure for `sso_oauth_auth_codes`
-- ----------------------------
DROP TABLE IF EXISTS `sso_oauth_auth_codes`;
CREATE TABLE `sso_oauth_auth_codes` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` int(10) unsigned NOT NULL,
  `redirect_uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_session_id_index` (`session_id`),
  CONSTRAINT `oauth_auth_codes_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sso_oauth_sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sso_oauth_auth_codes
-- ----------------------------
INSERT INTO `sso_oauth_auth_codes` VALUES ('5oiJEK2oUNdHazHTMEXid4uDR12tINRkaD2QtcEW', '63', 'http://www.opcache2.net/login_back.html', '1444962340', '2015-10-16 01:25:40', '2015-10-16 01:25:40');
INSERT INTO `sso_oauth_auth_codes` VALUES ('9bsXDZKGvFY6zXpPHes4Mpoc5T9h47BmYEmuKPs9', '67', 'http://www.opcache2.net/login_back.html', '1444964590', '2015-10-16 02:03:10', '2015-10-16 02:03:10');
INSERT INTO `sso_oauth_auth_codes` VALUES ('bTnH1y7z6D8Z6gZ4ARlVa3hfi8H9FBkYbSphLQiE', '69', 'http://www.opcache.net/login_back.html', '1444964693', '2015-10-16 02:04:53', '2015-10-16 02:04:53');
INSERT INTO `sso_oauth_auth_codes` VALUES ('M4AOxLKYaAZFNbrrkiDNtQ5S0vcWsoZd5Ad6vyWo', '62', 'http://www.opcache2.net/login_back.html', '1444962192', '2015-10-16 01:23:12', '2015-10-16 01:23:12');
INSERT INTO `sso_oauth_auth_codes` VALUES ('pQlaA2f2ozFdlDuRUEaxtS8pv46XHYFwS5KzucBw', '61', 'http://www.opcache.net/login_back.html', '1444962066', '2015-10-16 01:21:06', '2015-10-16 01:21:06');
INSERT INTO `sso_oauth_auth_codes` VALUES ('RmrRnmXZJKVocZhXEBT5EI5IN1x4TKKy0tN4EZ3A', '60', 'http://www.opcache.net/login_back.html', '1444961881', '2015-10-16 01:18:01', '2015-10-16 01:18:01');
INSERT INTO `sso_oauth_auth_codes` VALUES ('tgGxvK2mmMvlJCXEoFWC4Y2LTkCZ8M5EnvXTgUhD', '66', 'http://www.opcache2.net/login_back.html', '1444962711', '2015-10-16 01:31:51', '2015-10-16 01:31:51');
INSERT INTO `sso_oauth_auth_codes` VALUES ('Wo14kS2nacXobNL2AZeWsczxBadfRndULbIsZHUh', '65', 'http://www.opcache2.net/login_back.html', '1444962512', '2015-10-16 01:28:32', '2015-10-16 01:28:32');
INSERT INTO `sso_oauth_auth_codes` VALUES ('wPaTOUTS2HT3hPuPtIFIMEbutWM5kvPjHhb9XQDu', '64', 'http://www.opcache2.net/login_back.html', '1444962463', '2015-10-16 01:27:43', '2015-10-16 01:27:43');
INSERT INTO `sso_oauth_auth_codes` VALUES ('yw1wS4OcjSv38AqQ2Kyq8EXNTmm8y1krdfdAH4ar', '70', 'http://www.opcache.net/login_back.html', '1444964749', '2015-10-16 02:05:49', '2015-10-16 02:05:49');

-- ----------------------------
-- Table structure for `sso_oauth_client_endpoints`
-- ----------------------------
DROP TABLE IF EXISTS `sso_oauth_client_endpoints`;
CREATE TABLE `sso_oauth_client_endpoints` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `redirect_uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth_client_endpoints_client_id_redirect_uri_unique` (`client_id`,`redirect_uri`),
  CONSTRAINT `oauth_client_endpoints_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `sso_oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sso_oauth_client_endpoints
-- ----------------------------
INSERT INTO `sso_oauth_client_endpoints` VALUES ('1', 's6BhdRkqt3', 'http://www.opcache.net/login_back.html', '2015-08-14 13:47:41', '2015-08-14 13:47:44');
INSERT INTO `sso_oauth_client_endpoints` VALUES ('5', 'asdfsadf1', 'http://www.opcache2.net/login_back.html', '2015-10-15 08:11:30', '2015-10-20 03:29:39');

-- ----------------------------
-- Table structure for `sso_oauth_client_grants`
-- ----------------------------
DROP TABLE IF EXISTS `sso_oauth_client_grants`;
CREATE TABLE `sso_oauth_client_grants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `grant_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `oauth_client_grants_client_id_index` (`client_id`),
  KEY `oauth_client_grants_grant_id_index` (`grant_id`),
  CONSTRAINT `oauth_client_grants_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `sso_oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `oauth_client_grants_grant_id_foreign` FOREIGN KEY (`grant_id`) REFERENCES `sso_oauth_grants` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sso_oauth_client_grants
-- ----------------------------

-- ----------------------------
-- Table structure for `sso_oauth_client_scopes`
-- ----------------------------
DROP TABLE IF EXISTS `sso_oauth_client_scopes`;
CREATE TABLE `sso_oauth_client_scopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `oauth_client_scopes_client_id_index` (`client_id`),
  KEY `oauth_client_scopes_scope_id_index` (`scope_id`),
  CONSTRAINT `oauth_client_scopes_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `sso_oauth_clients` (`id`) ON DELETE CASCADE,
  CONSTRAINT `oauth_client_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `sso_oauth_scopes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sso_oauth_client_scopes
-- ----------------------------

-- ----------------------------
-- Table structure for `sso_oauth_clients`
-- ----------------------------
DROP TABLE IF EXISTS `sso_oauth_clients`;
CREATE TABLE `sso_oauth_clients` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth_clients_id_secret_unique` (`id`,`secret`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sso_oauth_clients
-- ----------------------------
INSERT INTO `sso_oauth_clients` VALUES ('asdfsadf1', 'sdfasdf1231231', 'test1', '2015-10-15 08:11:30', '2015-10-15 08:11:30');
INSERT INTO `sso_oauth_clients` VALUES ('s6BhdRkqt3', 'addfsdf123', 'test', '2015-08-14 13:40:41', '2015-08-14 13:40:45');

-- ----------------------------
-- Table structure for `sso_oauth_grant_scopes`
-- ----------------------------
DROP TABLE IF EXISTS `sso_oauth_grant_scopes`;
CREATE TABLE `sso_oauth_grant_scopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grant_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `oauth_grant_scopes_grant_id_index` (`grant_id`),
  KEY `oauth_grant_scopes_scope_id_index` (`scope_id`),
  CONSTRAINT `oauth_grant_scopes_grant_id_foreign` FOREIGN KEY (`grant_id`) REFERENCES `sso_oauth_grants` (`id`) ON DELETE CASCADE,
  CONSTRAINT `oauth_grant_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `sso_oauth_scopes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sso_oauth_grant_scopes
-- ----------------------------

-- ----------------------------
-- Table structure for `sso_oauth_grants`
-- ----------------------------
DROP TABLE IF EXISTS `sso_oauth_grants`;
CREATE TABLE `sso_oauth_grants` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sso_oauth_grants
-- ----------------------------

-- ----------------------------
-- Table structure for `sso_oauth_refresh_tokens`
-- ----------------------------
DROP TABLE IF EXISTS `sso_oauth_refresh_tokens`;
CREATE TABLE `sso_oauth_refresh_tokens` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`access_token_id`),
  UNIQUE KEY `oauth_refresh_tokens_id_unique` (`id`),
  CONSTRAINT `oauth_refresh_tokens_access_token_id_foreign` FOREIGN KEY (`access_token_id`) REFERENCES `sso_oauth_access_tokens` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sso_oauth_refresh_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for `sso_oauth_scopes`
-- ----------------------------
DROP TABLE IF EXISTS `sso_oauth_scopes`;
CREATE TABLE `sso_oauth_scopes` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sso_oauth_scopes
-- ----------------------------

-- ----------------------------
-- Table structure for `sso_oauth_session_scopes`
-- ----------------------------
DROP TABLE IF EXISTS `sso_oauth_session_scopes`;
CREATE TABLE `sso_oauth_session_scopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(10) unsigned NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `oauth_session_scopes_session_id_index` (`session_id`),
  KEY `oauth_session_scopes_scope_id_index` (`scope_id`),
  CONSTRAINT `oauth_session_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `sso_oauth_scopes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `oauth_session_scopes_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sso_oauth_sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sso_oauth_session_scopes
-- ----------------------------

-- ----------------------------
-- Table structure for `sso_oauth_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `sso_oauth_sessions`;
CREATE TABLE `sso_oauth_sessions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `owner_type` enum('client','user') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `owner_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_redirect_uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `oauth_sessions_client_id_owner_type_owner_id_index` (`client_id`,`owner_type`,`owner_id`),
  CONSTRAINT `oauth_sessions_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `sso_oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sso_oauth_sessions
-- ----------------------------
INSERT INTO `sso_oauth_sessions` VALUES ('17', 's6BhdRkqt3', 'user', '55', 'http://oauth_client.opcache.net:8080', '2015-08-18 02:05:17', '2015-08-18 02:05:17');
INSERT INTO `sso_oauth_sessions` VALUES ('18', 's6BhdRkqt3', 'user', '55', 'http://www.opcache.net/login_back.html', '2015-10-14 08:33:15', '2015-10-14 08:33:15');
INSERT INTO `sso_oauth_sessions` VALUES ('19', 's6BhdRkqt3', 'user', '55', 'http://www.opcache.net/login_back.html', '2015-10-14 08:43:30', '2015-10-14 08:43:30');
INSERT INTO `sso_oauth_sessions` VALUES ('20', 's6BhdRkqt3', 'user', '55', 'http://www.opcache.net/login_back.html', '2015-10-14 09:04:44', '2015-10-14 09:04:44');
INSERT INTO `sso_oauth_sessions` VALUES ('21', 's6BhdRkqt3', 'user', '55', 'http://www.opcache.net/login_back.html', '2015-10-14 09:06:11', '2015-10-14 09:06:11');
INSERT INTO `sso_oauth_sessions` VALUES ('22', 's6BhdRkqt3', 'user', '55', 'http://www.opcache.net/login_back.html', '2015-10-14 09:07:34', '2015-10-14 09:07:34');
INSERT INTO `sso_oauth_sessions` VALUES ('23', 's6BhdRkqt3', 'user', '55', 'http://www.opcache.net/login_back.html', '2015-10-14 09:10:49', '2015-10-14 09:10:49');
INSERT INTO `sso_oauth_sessions` VALUES ('24', 's6BhdRkqt3', 'user', '55', 'http://www.opcache.net/login_back.html', '2015-10-14 09:14:39', '2015-10-14 09:14:39');
INSERT INTO `sso_oauth_sessions` VALUES ('25', 's6BhdRkqt3', 'user', '55', 'http://www.opcache.net/login_back.html', '2015-10-15 01:23:17', '2015-10-15 01:23:17');
INSERT INTO `sso_oauth_sessions` VALUES ('26', 's6BhdRkqt3', 'user', '55', 'http://www.opcache.net/login_back.html', '2015-10-15 01:32:13', '2015-10-15 01:32:13');
INSERT INTO `sso_oauth_sessions` VALUES ('27', 's6BhdRkqt3', 'user', '55', 'http://www.opcache.net/login_back.html', '2015-10-15 05:02:35', '2015-10-15 05:02:35');
INSERT INTO `sso_oauth_sessions` VALUES ('28', 's6BhdRkqt3', 'user', '55', 'http://www.opcache.net/login_back.html', '2015-10-15 05:32:26', '2015-10-15 05:32:26');
INSERT INTO `sso_oauth_sessions` VALUES ('29', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 05:48:33', '2015-10-15 05:48:33');
INSERT INTO `sso_oauth_sessions` VALUES ('30', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 05:48:55', '2015-10-15 05:48:55');
INSERT INTO `sso_oauth_sessions` VALUES ('31', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 05:54:27', '2015-10-15 05:54:27');
INSERT INTO `sso_oauth_sessions` VALUES ('32', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 05:56:18', '2015-10-15 05:56:18');
INSERT INTO `sso_oauth_sessions` VALUES ('33', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 06:04:46', '2015-10-15 06:04:46');
INSERT INTO `sso_oauth_sessions` VALUES ('34', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 06:05:56', '2015-10-15 06:05:56');
INSERT INTO `sso_oauth_sessions` VALUES ('35', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 06:06:19', '2015-10-15 06:06:19');
INSERT INTO `sso_oauth_sessions` VALUES ('36', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 06:07:31', '2015-10-15 06:07:31');
INSERT INTO `sso_oauth_sessions` VALUES ('37', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 06:07:48', '2015-10-15 06:07:48');
INSERT INTO `sso_oauth_sessions` VALUES ('38', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 06:09:38', '2015-10-15 06:09:38');
INSERT INTO `sso_oauth_sessions` VALUES ('39', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 06:11:56', '2015-10-15 06:11:56');
INSERT INTO `sso_oauth_sessions` VALUES ('40', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 06:13:08', '2015-10-15 06:13:08');
INSERT INTO `sso_oauth_sessions` VALUES ('41', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 08:31:24', '2015-10-15 08:31:24');
INSERT INTO `sso_oauth_sessions` VALUES ('42', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 08:32:56', '2015-10-15 08:32:56');
INSERT INTO `sso_oauth_sessions` VALUES ('43', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 08:33:00', '2015-10-15 08:33:00');
INSERT INTO `sso_oauth_sessions` VALUES ('44', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 08:33:08', '2015-10-15 08:33:08');
INSERT INTO `sso_oauth_sessions` VALUES ('45', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 08:34:04', '2015-10-15 08:34:04');
INSERT INTO `sso_oauth_sessions` VALUES ('46', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 08:35:44', '2015-10-15 08:35:44');
INSERT INTO `sso_oauth_sessions` VALUES ('47', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 08:35:49', '2015-10-15 08:35:49');
INSERT INTO `sso_oauth_sessions` VALUES ('48', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 08:36:51', '2015-10-15 08:36:51');
INSERT INTO `sso_oauth_sessions` VALUES ('49', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 08:37:01', '2015-10-15 08:37:01');
INSERT INTO `sso_oauth_sessions` VALUES ('50', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 08:37:26', '2015-10-15 08:37:26');
INSERT INTO `sso_oauth_sessions` VALUES ('51', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 08:41:02', '2015-10-15 08:41:02');
INSERT INTO `sso_oauth_sessions` VALUES ('52', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 08:42:40', '2015-10-15 08:42:40');
INSERT INTO `sso_oauth_sessions` VALUES ('53', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 08:43:10', '2015-10-15 08:43:10');
INSERT INTO `sso_oauth_sessions` VALUES ('54', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 08:44:21', '2015-10-15 08:44:21');
INSERT INTO `sso_oauth_sessions` VALUES ('55', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 08:44:40', '2015-10-15 08:44:40');
INSERT INTO `sso_oauth_sessions` VALUES ('56', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 08:57:15', '2015-10-15 08:57:15');
INSERT INTO `sso_oauth_sessions` VALUES ('57', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 09:02:33', '2015-10-15 09:02:33');
INSERT INTO `sso_oauth_sessions` VALUES ('58', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 09:02:45', '2015-10-15 09:02:45');
INSERT INTO `sso_oauth_sessions` VALUES ('59', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-15 09:03:42', '2015-10-15 09:03:42');
INSERT INTO `sso_oauth_sessions` VALUES ('60', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-16 01:18:01', '2015-10-16 01:18:01');
INSERT INTO `sso_oauth_sessions` VALUES ('61', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-16 01:21:06', '2015-10-16 01:21:06');
INSERT INTO `sso_oauth_sessions` VALUES ('62', 'asdfsadf1', 'user', '1', 'http://www.opcache2.net/login_back.html', '2015-10-16 01:23:12', '2015-10-16 01:23:12');
INSERT INTO `sso_oauth_sessions` VALUES ('63', 'asdfsadf1', 'user', '1', 'http://www.opcache2.net/login_back.html', '2015-10-16 01:25:40', '2015-10-16 01:25:40');
INSERT INTO `sso_oauth_sessions` VALUES ('64', 'asdfsadf1', 'user', '1', 'http://www.opcache2.net/login_back.html', '2015-10-16 01:27:43', '2015-10-16 01:27:43');
INSERT INTO `sso_oauth_sessions` VALUES ('65', 'asdfsadf1', 'user', '1', 'http://www.opcache2.net/login_back.html', '2015-10-16 01:28:32', '2015-10-16 01:28:32');
INSERT INTO `sso_oauth_sessions` VALUES ('66', 'asdfsadf1', 'user', '1', 'http://www.opcache2.net/login_back.html', '2015-10-16 01:31:51', '2015-10-16 01:31:51');
INSERT INTO `sso_oauth_sessions` VALUES ('67', 'asdfsadf1', 'user', '1', 'http://www.opcache2.net/login_back.html', '2015-10-16 02:03:10', '2015-10-16 02:03:10');
INSERT INTO `sso_oauth_sessions` VALUES ('68', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-16 02:04:11', '2015-10-16 02:04:11');
INSERT INTO `sso_oauth_sessions` VALUES ('69', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-16 02:04:53', '2015-10-16 02:04:53');
INSERT INTO `sso_oauth_sessions` VALUES ('70', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-16 02:05:49', '2015-10-16 02:05:49');
INSERT INTO `sso_oauth_sessions` VALUES ('71', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-16 02:22:44', '2015-10-16 02:22:44');
INSERT INTO `sso_oauth_sessions` VALUES ('72', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-16 02:29:30', '2015-10-16 02:29:30');
INSERT INTO `sso_oauth_sessions` VALUES ('73', 'asdfsadf1', 'user', '1', 'http://www.opcache2.net/login_back.html', '2015-10-16 02:29:47', '2015-10-16 02:29:47');
INSERT INTO `sso_oauth_sessions` VALUES ('74', 'asdfsadf1', 'user', '1', 'http://www.opcache2.net/login_back.html', '2015-10-16 02:33:27', '2015-10-16 02:33:27');
INSERT INTO `sso_oauth_sessions` VALUES ('75', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-16 02:33:39', '2015-10-16 02:33:39');
INSERT INTO `sso_oauth_sessions` VALUES ('76', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-19 02:29:23', '2015-10-19 02:29:23');
INSERT INTO `sso_oauth_sessions` VALUES ('77', 'asdfsadf1', 'user', '1', 'http://www.opcache2.net/login_back.html', '2015-10-19 02:29:33', '2015-10-19 02:29:33');
INSERT INTO `sso_oauth_sessions` VALUES ('78', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-19 05:35:13', '2015-10-19 05:35:13');
INSERT INTO `sso_oauth_sessions` VALUES ('79', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-19 05:36:17', '2015-10-19 05:36:17');
INSERT INTO `sso_oauth_sessions` VALUES ('80', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-19 05:36:34', '2015-10-19 05:36:34');
INSERT INTO `sso_oauth_sessions` VALUES ('81', 'asdfsadf1', 'user', '1', 'http://www.opcache2.net/login_back.html', '2015-10-20 01:00:19', '2015-10-20 01:00:19');
INSERT INTO `sso_oauth_sessions` VALUES ('82', 'asdfsadf1', 'user', '1', 'http://www.opcache2.net/login_back.html', '2015-10-20 01:54:32', '2015-10-20 01:54:32');
INSERT INTO `sso_oauth_sessions` VALUES ('83', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-20 03:20:14', '2015-10-20 03:20:14');
INSERT INTO `sso_oauth_sessions` VALUES ('84', 's6BhdRkqt3', 'user', '1', 'http://www.opcache.net/login_back.html', '2015-10-20 03:20:42', '2015-10-20 03:20:42');
INSERT INTO `sso_oauth_sessions` VALUES ('85', 's6BhdRkqt3', 'user', '6', 'http://www.opcache.net/login_back.html', '2015-10-20 03:21:08', '2015-10-20 03:21:08');
INSERT INTO `sso_oauth_sessions` VALUES ('86', 's6BhdRkqt3', 'user', '6', 'http://www.opcache.net/login_back.html', '2015-10-20 03:39:30', '2015-10-20 03:39:30');
INSERT INTO `sso_oauth_sessions` VALUES ('87', 's6BhdRkqt3', 'user', '6', 'http://www.opcache.net/login_back.html', '2015-10-20 03:50:58', '2015-10-20 03:50:58');
INSERT INTO `sso_oauth_sessions` VALUES ('88', 's6BhdRkqt3', 'user', '9', 'http://www.opcache.net/login_back.html', '2015-10-20 03:51:18', '2015-10-20 03:51:18');

-- ----------------------------
-- Table structure for `sso_permission`
-- ----------------------------
DROP TABLE IF EXISTS `sso_permission`;
CREATE TABLE `sso_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(255) NOT NULL COMMENT '模块',
  `class` varchar(255) NOT NULL COMMENT '类',
  `action` varchar(255) NOT NULL COMMENT '函数',
  `name` varchar(255) NOT NULL COMMENT '节点的名字',
  `display` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1为显示为菜单，0则不显示',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '节点的父节点，此值一般用于输出树形结构，0则为顶级',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `level` tinyint(2) NOT NULL DEFAULT '1' COMMENT '第几级菜单',
  `mark` varchar(255) NOT NULL COMMENT '备注',
  `add_time` bigint(20) NOT NULL COMMENT '增加的日期',
  PRIMARY KEY (`id`),
  KEY `module` (`module`) USING BTREE,
  KEY `class` (`class`) USING BTREE,
  KEY `action` (`action`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8 COMMENT='权限节点表_by_jiang';

-- ----------------------------
-- Records of sso_permission
-- ----------------------------
INSERT INTO `sso_permission` VALUES ('1', 'foundation', '系统管理', '系统管理', '系统管理', '1', '0', '0', '1', '系统管理页面，不作权限验证，只用做菜单显示', '0');
INSERT INTO `sso_permission` VALUES ('2', 'foundation', 'group', 'index', '用户组管理', '1', '67', '2', '3', '用户组管理页面', '0');
INSERT INTO `sso_permission` VALUES ('3', 'foundation', 'acl', 'index', '功能管理', '1', '67', '1', '3', '功能管理页面', '0');
INSERT INTO `sso_permission` VALUES ('4', 'foundation', 'user', 'index', '用户管理', '1', '67', '3', '3', '用户管理页面', '0');
INSERT INTO `sso_permission` VALUES ('20', 'foundation', 'user', 'add', '增加用户', '0', '4', '0', '4', '增加一个用户', '0');
INSERT INTO `sso_permission` VALUES ('23', 'foundation', 'group', 'add', '增加用户组', '0', '2', '0', '4', '增加用户组', '1406882443');
INSERT INTO `sso_permission` VALUES ('24', 'foundation', 'group', 'edit', '用户组编辑', '0', '2', '0', '4', '用户组编辑', '1406882515');
INSERT INTO `sso_permission` VALUES ('25', 'foundation', 'group', 'delete', '用户组删除', '0', '2', '0', '4', '用户组删除、批量删除', '1406882542');
INSERT INTO `sso_permission` VALUES ('26', 'foundation', 'acl', 'group', '用户组权限管理', '0', '2', '0', '4', '用户组权限管理', '1406882568');
INSERT INTO `sso_permission` VALUES ('27', 'foundation', 'user', 'edit', '用户编辑', '0', '4', '0', '4', '用户编辑', '1406882640');
INSERT INTO `sso_permission` VALUES ('28', 'foundation', 'user', 'delete', '用户删除', '0', '4', '0', '4', '用户删除', '1406882664');
INSERT INTO `sso_permission` VALUES ('29', 'foundation', 'acl', 'user', '用户权限管理', '0', '4', '0', '4', '用户权限管理、设置用户权限', '1406882698');
INSERT INTO `sso_permission` VALUES ('30', 'foundation', 'acl', 'add', '增加功能菜单', '0', '3', '0', '4', '增加功能菜单', '1406882729');
INSERT INTO `sso_permission` VALUES ('31', 'foundation', 'acl', 'edit', '功能菜单编辑', '0', '3', '0', '4', '功能菜单编辑', '1406882754');
INSERT INTO `sso_permission` VALUES ('32', 'foundation', 'acl', 'delete', '功能菜单删除', '0', '3', '0', '4', '功能菜单删除', '1406882775');
INSERT INTO `sso_permission` VALUES ('33', 'foundation', 'acl', 'sort', '功能菜单排序', '0', '3', '0', '4', '功能菜单排序', '1406882815');
INSERT INTO `sso_permission` VALUES ('43', 'foundation', 'index', 'cs', '功能示例', '1', '1', '0', '2', '一些小功能的合集，可以用来加快开发的速度。', '1427788812');
INSERT INTO `sso_permission` VALUES ('44', 'foundation', 'upload', 'index', '弹出窗口上传', '0', '66', '0', '2', '通用的弹出窗口上传。', '1427790345');
INSERT INTO `sso_permission` VALUES ('53', 'foundation', 'log', 'action', '操作日志', '1', '68', '0', '3', '查看操作日志', '1433319136');
INSERT INTO `sso_permission` VALUES ('55', '工作流管理', '工作流管理', '工作流管理', '工作流管理', '1', '1', '0', '2', '', '1434093108');
INSERT INTO `sso_permission` VALUES ('56', 'workflow', 'index', 'add', '工作流增加', '0', '73', '0', '4', '增加新的工作流程', '1434333268');
INSERT INTO `sso_permission` VALUES ('57', 'workflow', 'index', 'edit', '工作流编辑', '0', '73', '0', '4', '修改工作流信息', '1434336714');
INSERT INTO `sso_permission` VALUES ('58', 'workflow', 'index', 'delete', '工作流删除', '0', '73', '0', '4', '删除工作流', '1434338279');
INSERT INTO `sso_permission` VALUES ('59', 'workflow', 'step', 'index', '工作流详情', '0', '55', '1', '3', '查看工作流详情', '1434345119');
INSERT INTO `sso_permission` VALUES ('60', 'workflow', 'step', 'add', '增加工作流步骤', '0', '59', '0', '4', '增加工作流步骤', '1434349481');
INSERT INTO `sso_permission` VALUES ('61', 'workflow', 'step', 'edit', '编辑工作流步骤', '0', '59', '0', '4', '编辑工作流步骤', '1434353110');
INSERT INTO `sso_permission` VALUES ('62', 'workflow', 'step', 'delete', '工作流步骤删除', '0', '59', '0', '4', '工作流步骤删除', '1434354926');
INSERT INTO `sso_permission` VALUES ('63', 'workflow', 'step', 'relation', '工作流设置关联人员', '0', '59', '0', '4', '工作流设置关联人员', '1434422499');
INSERT INTO `sso_permission` VALUES ('66', '通用功能', '通用功能', '通用功能', '通用功能', '0', '0', '0', '1', '通用功能，一般会开发这些功能给用户。', '1435545336');
INSERT INTO `sso_permission` VALUES ('67', '用户与权限管理', '用户与权限管理', '用户与权限管理', '用户与权限管理', '1', '1', '0', '2', '包括功能用户管理、用户组管理、功能管理，权限管理。', '1436147892');
INSERT INTO `sso_permission` VALUES ('68', '系统日志', '系统日志', '系统日志', '系统日志', '1', '1', '0', '2', '主要是各类的系统日志', '1436147908');
INSERT INTO `sso_permission` VALUES ('73', 'workflow', 'index', 'index', '工作流列表', '1', '55', '2', '3', '', '1436232634');
INSERT INTO `sso_permission` VALUES ('83', '应用管理', '应用管理', '应用管理', '应用管理', '1', '0', '0', '1', '应用管理', '1444810847');
INSERT INTO `sso_permission` VALUES ('84', 'app', 'manager', 'index', '应用信息管理', '1', '83', '0', '2', '', '1444810873');
INSERT INTO `sso_permission` VALUES ('85', 'app', 'manager', 'add', '增加应用', '0', '84', '0', '3', '', '1444891391');
INSERT INTO `sso_permission` VALUES ('86', 'app', 'manager', 'delete', '删除应用', '0', '84', '0', '3', '', '1444893603');
INSERT INTO `sso_permission` VALUES ('87', 'app', 'manager', 'edit', '编辑应用', '0', '84', '0', '3', '', '1444893632');

-- ----------------------------
-- Table structure for `sso_users`
-- ----------------------------
DROP TABLE IF EXISTS `sso_users`;
CREATE TABLE `sso_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '用户密码',
  `group_id` int(11) NOT NULL,
  `realname` varchar(255) NOT NULL DEFAULT '' COMMENT '真实性名',
  `token` varchar(255) NOT NULL COMMENT '用户注册时的密钥',
  `add_time` bigint(20) NOT NULL COMMENT '用户注册的时间',
  `modify_time` bigint(20) NOT NULL COMMENT '用户信息所修改的时间',
  `mobile` varchar(11) NOT NULL COMMENT '手机',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户禁用0正常的1',
  `mark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `last_login_ip` varchar(255) NOT NULL COMMENT '最后登录ip',
  `last_login_time` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE,
  KEY `password` (`password`) USING BTREE,
  KEY `group_id` (`group_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='用户表_by_jiang';

-- ----------------------------
-- Records of sso_users
-- ----------------------------
INSERT INTO `sso_users` VALUES ('1', 'admin', '6512bd43d9caa6e02c990b0a82652dca', '1', '管理员', 'oyzzO7YxmgJHlAfdK5HaZMscegJPcTrw5drPQRS6bjlfAkTB6NELPvqpc12q', '0', '0', '12', '1', '超级用户组', '127.0.0.1', '1445311825');
INSERT INTO `sso_users` VALUES ('2', 'test', 'c4ca4238a0b923820dcc509a6f75849b', '2', 'test', '', '1436169648', '0', '11', '1', '1', '127.0.0.1', '1436241545');
INSERT INTO `sso_users` VALUES ('3', 'test2', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', '', '1436170910', '0', '1', '1', '1', '', '0');

-- ----------------------------
-- Table structure for `sso_workflow`
-- ----------------------------
DROP TABLE IF EXISTS `sso_workflow`;
CREATE TABLE `sso_workflow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '工作流的名字',
  `description` text NOT NULL COMMENT '描述',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '数据插入的时间',
  `code` varchar(20) NOT NULL COMMENT '调用字符串，用于与程序结合',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '工作流的类型，1为多用户的类OA审核，2为辅助权限',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`) USING BTREE,
  KEY `type` (`type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='工作流表';

-- ----------------------------
-- Records of sso_workflow
-- ----------------------------
INSERT INTO `sso_workflow` VALUES ('8', '测试工作流', '测试工作流2', '1434508916', 'W_sdfg', '1');
INSERT INTO `sso_workflow` VALUES ('10', '辅助权限测试2', '辅助权限测试2a', '1436510712', 'W_dd', '2');

-- ----------------------------
-- Table structure for `sso_workflow_step`
-- ----------------------------
DROP TABLE IF EXISTS `sso_workflow_step`;
CREATE TABLE `sso_workflow_step` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) NOT NULL COMMENT '所属的工作流',
  `name` varchar(255) NOT NULL COMMENT '工作流步骤的名称',
  `description` text NOT NULL COMMENT '工作流步骤的描述',
  `step_level` tinyint(4) NOT NULL DEFAULT '0' COMMENT '该工作流步骤所处的第几步,如果为99代表已经审核完成',
  `code` varchar(20) NOT NULL COMMENT '主要用于权限辅助调用',
  `addtime` int(11) NOT NULL COMMENT '数据增加的日期',
  PRIMARY KEY (`id`),
  KEY `workflow_id` (`workflow_id`) USING BTREE,
  KEY `step_level` (`step_level`) USING BTREE,
  KEY `code` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='工作流的详细步骤';

-- ----------------------------
-- Records of sso_workflow_step
-- ----------------------------
INSERT INTO `sso_workflow_step` VALUES ('9', '8', '测试1', '1', '1', '', '1434516742');
INSERT INTO `sso_workflow_step` VALUES ('10', '8', '测试2', '2', '2', '', '1434516769');
INSERT INTO `sso_workflow_step` VALUES ('15', '10', '测试辅助权限工作流步骤', '测试辅助权限工作流步骤a', '0', 'W_sdfsf', '1436510782');

-- ----------------------------
-- Table structure for `sso_workflow_user`
-- ----------------------------
DROP TABLE IF EXISTS `sso_workflow_user`;
CREATE TABLE `sso_workflow_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_step_id` int(11) NOT NULL COMMENT '工作流步骤的ID',
  `user_id` int(11) NOT NULL COMMENT '后台管理员的ID',
  `workflow_id` int(11) NOT NULL COMMENT '工作流ID',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `workflow_step_id` (`workflow_step_id`) USING BTREE,
  KEY `workflow_id` (`workflow_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sso_workflow_user
-- ----------------------------
INSERT INTO `sso_workflow_user` VALUES ('19', '9', '1', '8');
INSERT INTO `sso_workflow_user` VALUES ('23', '10', '3', '8');
INSERT INTO `sso_workflow_user` VALUES ('27', '15', '2', '10');
INSERT INTO `sso_workflow_user` VALUES ('28', '15', '1', '10');
